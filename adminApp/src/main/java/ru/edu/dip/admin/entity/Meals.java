package ru.edu.dip.admin.entity;

import javax.persistence.*;

@Entity
@Table(name="meals")
public class Meals {

    @Id
    @Column(name="id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String title;
    private String price;
    private String description;
    private String type;
    private String linkImage;

    @Transient
    private String status;

    public Meals () {

    }

    public Meals(String title, String price, String description, String type, String linkImage, String status) {
        this.title = title;
        this.price = price;
        this.description = description;
        this.type = type;
        this.linkImage = linkImage;
        this.status = status;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Meals(String title, String price, String description, String type, String linkImage) {
        this.title = title;
        this.price = price;
        this.description = description;
        this.type = type;
        this.linkImage = linkImage;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getLinkImage() {
        return linkImage;
    }

    public void setLinkImage(String linkImage) {
        this.linkImage = linkImage;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Meals{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", price='" + price + '\'' +
                ", description='" + description + '\'' +
                ", type='" + type + '\'' +
                ", linkImage='" + linkImage + '\'' +
                ", status='" + status + '\'' +
                '}';
    }
}
