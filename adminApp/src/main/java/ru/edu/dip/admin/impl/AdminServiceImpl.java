package ru.edu.dip.admin.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.edu.dip.admin.AdminService;
import ru.edu.dip.admin.UserRepository;
import ru.edu.dip.admin.entity.Booking;
import ru.edu.dip.admin.entity.DietTable;
import ru.edu.dip.admin.entity.Meals;

import java.util.*;

@Service
public class AdminServiceImpl implements AdminService {

    private final Set<String> allTime = new HashSet<>(Arrays.asList("12:00", "13:00", "14:00", "15:00", "16:00", "17:00", "18:00", "19:00"));
    private ConfirmTime confirmTime = new ConfirmTime();
    private UserRepository adminRepository;

    @Autowired
    public AdminServiceImpl(UserRepository adminRepository) {
        this.adminRepository = adminRepository;
    }

    /**
     * Получение всего воозможного времени бронирования.
     *
     * @return
     */
    @Override
    public Set<String> getAllTime() {
        return allTime;
    }

    /**
     * Получение стола по id.
     *
     * @param id
     * @return
     */
    @Override
    public DietTable getTable(int id) {
        DietTable tableMain = null;
        for (DietTable table : adminRepository.getTables()) {
            if (table.getId() == id) {
                tableMain = table;
                return tableMain;
            }
        }
        if (tableMain == null){
            throw new IllegalArgumentException("Стол с данным id не существует");
        }
        return tableMain;
    }

    /**
     * Получение не занятых столов.
     *
     * @return
     */
    @Override
    public List<DietTable> getFreeTables() {
        List<DietTable> tables = new ArrayList<>();
        for (DietTable table : adminRepository.getTables()){
            if(!table.isStatus()){
                tables.add(table);
            }
        }
        return tables;
    }

    /**
     * Получение списка бронирования.
     *
     * @return
     */
    @Override
    public List<Booking> getBookingList() {
        return adminRepository.getBookingList();
    }

    /**
     * Добавление бронирования в список юронирования.
     *
     * @param booking
     */
    @Override
    public void addToBookingList(Booking booking) {
        adminRepository.addToBookingList(booking);
    }

    /**
     * Получение списка времени на которое забронированы столики.
     *
     * @return
     */
    @Override
    public Set<String> getAllBookingTime() {
        Set<String> allBookingTime = new HashSet<>();
        for (Booking booking : adminRepository.getBookingList()){
            allBookingTime.add(booking.getBookingTableTime());
        }
        return allBookingTime;
    }

    /**
     * Получение всех забронированных столов.
     *
     * @return
     */
    @Override
    public List<DietTable> getAllBookingTable() {
        List<DietTable> allBookingTable = new ArrayList<>();

        for (DietTable table : adminRepository.getTables()){
            if(table.isStatus()){
                allBookingTable.add(table);
            }
        }
        return allBookingTable;
    }

    /**
     * Получение всех имеющихся столов.
     *
     * @return
     */
    @Override
    public List<DietTable> getAllTables() {
        return adminRepository.getTables();
    }


    // Надо исправить чтобы отсыылался тоьлко Booking файл!!!
    @Override
    public ConfirmTime getConfirmTime() {
        return confirmTime;
    }
    @Override
    public void setConfirmTime(ConfirmTime confirmTime) {
        this.confirmTime = confirmTime;
    }

    // -----------------------------------------------------------------------------------

    /**
     * Получение списка всех блюд.
     *
     * @return
     */
    @Override
    public List<Meals> getMealsList() {
        return adminRepository.getMeals();
    }

    /**
     * Удаление блюда из списка.
     *
     * @param mealsId
     */
    @Override
    public void removeMealsInMenu(Long mealsId){
        adminRepository.deleteMeals(mealsId);
    }

    @Override
    public void addNewMealsToRepo(Meals meals){
        adminRepository.saveMeals(meals);
    }

    /**
     * Удаление бронирования по столу.
     *
     * @param numberOfTable
     */
    @Override
    public void removeBookingTable(int numberOfTable) {
        adminRepository.removeBookingTable(numberOfTable);
    }

    /**
     * Обновляет статус стола.
     *
     * @param numberOfTable
     */
    @Override
    public void updateTable(int numberOfTable) {
        adminRepository.updateTableStatus(numberOfTable);
    }

    /**
     * Обновление статуса заказа в списке бронированя.
     *
     * @param id
     */
    @Override
    public void updateBooking(int id) {
        adminRepository.updateBookingStatus(id);
    }


//    /**
//     * Свободное время
//     *
//     * @return freeTimeList.
//     */
//    @Override
//    public Set<String> getFreeTime(){
//
//        List<Table> tables = repository.getTables();
//        Set<String> freeTimeList = new HashSet<>();
//
//        for (Table table : tables){
//            freeTimeList.addAll(table.getFreeTime());
//        }
//
//        return freeTimeList;
//    }

//    /**
//     * Получение всех свободных мест по времени.
//     *
//     * @param time
//     * @return count
//     */
//    @Override
//    public int getFreePerson(String time){
//        int count = 0;
//        for (Table table : tables){
//            if (table.getFreeTime().contains(time)){
//                count += table.getPlace();
//            }
//        }
//        return count;
//    }

}
