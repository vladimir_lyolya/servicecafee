package ru.edu.dip.admin.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import ru.edu.dip.admin.AdminController;
import ru.edu.dip.admin.AdminService;
import ru.edu.dip.admin.entity.Booking;
import ru.edu.dip.admin.entity.DietTable;
import ru.edu.dip.admin.entity.Meals;
import ru.edu.dip.admin.entity.MealsMenu;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Controller
public class AdminControllerIml implements AdminController {

    public AdminService adminService;

    @Autowired
    public AdminControllerIml(AdminService adminService){
        this.adminService = adminService;
    }

    @RequestMapping("/login")
    public String login() {
        return "login";
    }

    @GetMapping(value = "/")
    public ModelAndView menu() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("StartMenuAdmin");
        return modelAndView;
    }

    @GetMapping("/bookingTables")
    public ModelAndView afterChoiceTime(){
        ModelAndView modelAndView = new ModelAndView();

        List<DietTable> tables = adminService.getFreeTables();
        Set<String> allTime = adminService.getAllTime();
        modelAndView.addObject("tables",tables);
        modelAndView.addObject("allTime", allTime);
        Booking booking = new Booking();
        modelAndView.addObject("booking", booking);
        modelAndView.setViewName("BookingTables");
        return modelAndView;
    }


    @PostMapping(value = "/resultBookingTable")
    public ModelAndView resultBookingTable(@ModelAttribute("booking") Booking booking) {
        booking.setBookingStatus("Ожидает подтверждения");
        adminService.addToBookingList(booking);
        // Получение и передача забронированного стола
        adminService.updateTable(booking.getNumberOfTable());
//        DietTable table = adminService.getTable(booking.getNumberOfTable());
//        table.setStatus(true);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("BookingResult");
        modelAndView.addObject("booking", booking);
        return modelAndView;
    }

    @GetMapping(value = "/confirm")
    public ModelAndView confirm() {
        Set<String> allBookingTime = adminService.getAllBookingTime();
//        List<Booking> bookingList = adminService.getBookingList();

        Set<String> allBookingTimeWithWaitingStatus = new HashSet<>();

        for (String s : allBookingTime){
            for (Booking booking : adminService.getBookingList()){
                if (booking.getBookingTableTime().equals(s) && booking.getBookingStatus().equals("Ожидает подтверждения")){
                    allBookingTimeWithWaitingStatus.add(booking.getBookingTableTime());
                }
            }
        }

        ConfirmTime confirmTime = new ConfirmTime();
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("Confirm");
//        modelAndView.addObject("bookingList", bookingList);
        modelAndView.addObject("allBookingTime", allBookingTimeWithWaitingStatus);
        modelAndView.addObject("confirmTime", confirmTime);
        return modelAndView;
    }

    @PostMapping(value = "/confirm")
    public ModelAndView viewConfirm(@ModelAttribute("confirmTime") ConfirmTime confirmTime) {

        List<Booking> allBookingList = adminService.getBookingList();
        List<Booking> allBookingListWithConfirmTime = new ArrayList<>();

        for (Booking booking : allBookingList){
            if (booking.getBookingStatus().equals("Ожидает подтверждения")  && booking.getBookingTableTime().equals(confirmTime.getConfirmTime())){
                allBookingListWithConfirmTime.add(booking);
            }
        }
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("allBookingListWithConfirmTime", allBookingListWithConfirmTime);
        Booking booking = new Booking();
        modelAndView.addObject("booking", booking);
        modelAndView.setViewName("ConfirmTable");
        return modelAndView;
    }

    @PostMapping(value = "confirmResult")
    public ModelAndView resultConfirm(@ModelAttribute("booking") Booking booking){
        ModelAndView modelAndView = new ModelAndView();

        adminService.updateBooking(booking.getNumberOfTable());
//        for (Booking booking1 : adminService.getBookingList()){
//            if(booking1.getNumberOfTable() == booking.getNumberOfTable()){
//                booking1.setBookingStatus("Подтвержден");
//            }
//        }
        modelAndView.addObject(booking);
        modelAndView.setViewName("ConfirmResult");
        return modelAndView;
    }
    @GetMapping(value = "/redacted")
    public ModelAndView redact() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("RedactedMenu");
        return modelAndView;
    }


    @GetMapping("/addDish")
    public ModelAndView addDish(){
        ModelAndView modelAndView = new ModelAndView();
        Meals meals = new Meals();
        modelAndView.addObject("meals", meals);
        modelAndView.setViewName("AddDish");

        return modelAndView;
    }

    @PostMapping("/addDishToMenu")
    public ModelAndView addDishToMenu(@ModelAttribute("meals") Meals meals){
        switch (meals.getType()) {
            case "soup":
                meals.setLinkImage("soupMeals.jpg");
                break;
            case "main":
                meals.setLinkImage("mainMeals.jpg");
                break;
            case "drink":
                meals.setLinkImage("coffee.jpg");
                break;
        }
        adminService.addNewMealsToRepo(meals);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("AddDish");
        return modelAndView;
    }

    @GetMapping("/removeDish")
    public ModelAndView removeDish(){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("RemoveDish");

        List<Meals> mealsFromRepo = adminService.getMealsList();
        List<Meals> soupMeals = new ArrayList<>();
        List<Meals> mainMeals = new ArrayList<>();
        List<Meals> drinkMeals = new ArrayList<>();
        for (Meals meals: mealsFromRepo) {
            if (meals.getType().equals("soup")) soupMeals.add(meals);
            if (meals.getType().equals("main")) mainMeals.add(meals);
            if (meals.getType().equals("drink")) drinkMeals.add(meals);
        }
        MealsMenu mealsMenu = new MealsMenu();
        mealsMenu.setSoupMeals(soupMeals);
        mealsMenu.setMainMeals(mainMeals);
        mealsMenu.setDrinkMeals(drinkMeals);
        mealsMenu.setUserChoice(null);
        modelAndView.addObject("mealsMenu",mealsMenu);

        return modelAndView;
    }

    @PostMapping("/removeDish")
    public ModelAndView removeDishFromMenu(@ModelAttribute("mealsMenu") MealsMenu mealsMenuRemove){
        ModelAndView modelAndView = new ModelAndView();
        Long removeMealsId = mealsMenuRemove.getUserChoice().getId();
        adminService.removeMealsInMenu(removeMealsId);

        List<Meals> mealsFromRepo = adminService.getMealsList();

        List<Meals> soupMeals = new ArrayList<>();
        List<Meals> mainMeals = new ArrayList<>();
        List<Meals> drinkMeals = new ArrayList<>();
        for (Meals meals: mealsFromRepo) {
            if (meals.getType().equals("soup")) soupMeals.add(meals);
            if (meals.getType().equals("main")) mainMeals.add(meals);
            if (meals.getType().equals("drink")) drinkMeals.add(meals);
        }
        MealsMenu mealsMenu = new MealsMenu();
        mealsMenu.setSoupMeals(soupMeals);
        mealsMenu.setMainMeals(mainMeals);
        mealsMenu.setDrinkMeals(drinkMeals);
        mealsMenu.setUserChoice(null);
        modelAndView.addObject("mealsMenu",mealsMenu);

        modelAndView.setViewName("RemoveDish");
        return modelAndView;
    }

    @GetMapping("/cancel")
    public ModelAndView cancelBooking(){
        ModelAndView modelAndView = new ModelAndView();
        List<Booking> bookingList = adminService.getBookingList();
        Booking cancelBooking = new Booking();
        modelAndView.addObject("cancelBooking", cancelBooking);
        modelAndView.addObject("bookingList", bookingList);
        modelAndView.setViewName("CancelBooking");
        return modelAndView;
    }

    @PostMapping("/cancel")
    public ModelAndView cancelBookingResult(@ModelAttribute("cancelBooking") Booking booking){
        ModelAndView modelAndView = new ModelAndView();
        adminService.removeBookingTable(booking.getNumberOfTable());
        adminService.updateTable(booking.getNumberOfTable());
        modelAndView.setViewName("CancelBooking");
        return modelAndView;
    }

    @GetMapping("/statistics")
    public ModelAndView statistics(){
        ModelAndView modelAndView = new ModelAndView();
        List<DietTable> freeTable = adminService.getFreeTables();
        List<Booking> bookingList = adminService.getBookingList();
        modelAndView.addObject("freeTableList", freeTable);
        modelAndView.addObject("bookingList", bookingList);
        modelAndView.setViewName("Statistics");
        return modelAndView;
    }

}
