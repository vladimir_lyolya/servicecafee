package ru.edu.dip.admin;

public interface SendMessage {

    public String sendMessage(String message);
    public void setDestination(String destination);
}
