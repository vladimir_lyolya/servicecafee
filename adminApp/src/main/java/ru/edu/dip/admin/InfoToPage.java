package ru.edu.dip.admin;


import ru.edu.dip.admin.entity.*;

import java.util.List;
import java.util.Set;

public class InfoToPage {

    private ListUserUI urnPage;
    private String message;
    private String codeActivation;
    private String codeActivationFromUser;
    private UserInfo userInfo;
    private MealsMenu mealsMenu;
    private UserMealsOrder userMealsOrder;
    private List<DietTable> tables;
    private Set<String> allTime;
    private Booking booking;

    public Booking getBooking() {
        return booking;
    }

    public void setBooking(Booking booking) {
        this.booking = booking;
    }

    public List<DietTable> getTables() {
        return tables;
    }

    public void setTables(List<DietTable> tables) {
        this.tables = tables;
    }

    public Set<String> getAllTime() {
        return allTime;
    }

    public void setAllTime(Set<String> allTime) {
        this.allTime = allTime;
    }

    public InfoToPage(){

    }

    public InfoToPage(ListUserUI urnPage, UserInfo userInfo,
                      String message, String codeActivation) {
        this.urnPage = urnPage;
        this.userInfo = userInfo;
        this.message = message;
        this.codeActivation = codeActivation;
    }

    public ListUserUI getUrnPage() {
        return urnPage;
    }

    public void setUrnPage(ListUserUI urnPage) {
        this.urnPage = urnPage;
    }

    public UserInfo getUserInfo() {
        return userInfo;
    }

    public void setUserInfo(UserInfo userInfo) {
        this.userInfo = userInfo;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCodeActivation() {
        return codeActivation;
    }

    public void setCodeActivation(String codeActivation) {
        this.codeActivation = codeActivation;
    }

    public String getCodeActivationFromUser() {
        return codeActivationFromUser;
    }

    public void setCodeActivationFromUser(String codeActivationFromUser) {
        this.codeActivationFromUser = codeActivationFromUser;
    }

    public MealsMenu getMealsMenu() {
        return mealsMenu;
    }

    public void setMealsMenu(MealsMenu mealsMenu) {
        this.mealsMenu = mealsMenu;
    }

    public UserMealsOrder getUserMealsOrder() {
        return userMealsOrder;
    }

    public void setUserMealsOrder(UserMealsOrder userMealsOrder) {
        this.userMealsOrder = userMealsOrder;
    }
}
