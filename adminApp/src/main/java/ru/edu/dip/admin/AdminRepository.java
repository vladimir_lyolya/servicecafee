package ru.edu.dip.admin;

import ru.edu.dip.admin.entity.Booking;
import ru.edu.dip.admin.entity.DietTable;
import ru.edu.dip.admin.entity.Meals;

import java.util.List;

public interface AdminRepository {

    public void createTable(DietTable table);
    public List<DietTable> getTables();
    public List<Booking> getBookingList();
    public void addToBookingList(Booking bookingList);
    public List<Meals> getRepoMeals();
    public void removeMealsInMenu(Long mealsId);
    public void addNewMealsToRepo(Meals meals);
    public void removeBookingTable(int numberOfTable);
}
