package ru.edu.dip.admin.convertor;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.core.convert.converter.Converter;
import ru.edu.dip.admin.entity.Meals;

public class StringToMeals implements Converter<String, Meals> {
    @Override
    public Meals convert(String from) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            Meals meals = mapper.readValue(from, Meals.class);
            return meals;
        } catch (JsonProcessingException e) {
            System.out.println("StringToMeals converted failed!" + e.getMessage());
            return new Meals();
        }
    }
}
