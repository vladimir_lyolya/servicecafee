package ru.edu.dip.admin;

import ru.edu.dip.admin.entity.Booking;
import ru.edu.dip.admin.entity.DietTable;
import ru.edu.dip.admin.entity.Meals;
import ru.edu.dip.admin.impl.ConfirmTime;

import java.util.List;
import java.util.Set;

public interface AdminService {

    public List<DietTable> getAllTables();
    public Set<String> getAllTime();
    public DietTable getTable(int id);
    public List<DietTable> getFreeTables();
    public List<Booking> getBookingList();
    public void addToBookingList(Booking booking);
    public Set<String> getAllBookingTime();
    public List<DietTable> getAllBookingTable();
    public ConfirmTime getConfirmTime();
    public void setConfirmTime(ConfirmTime confirmTime);
    public List<Meals> getMealsList();
    public void removeMealsInMenu(Long mealsId);
    public void addNewMealsToRepo(Meals meals);
    public void removeBookingTable(int numberOfTable);
    public void updateTable(int numberOfTable);

    public void updateBooking(int id);
}
