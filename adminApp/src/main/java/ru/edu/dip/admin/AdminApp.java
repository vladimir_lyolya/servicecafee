package ru.edu.dip.admin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 *
 *
 */
@SpringBootApplication
public class AdminApp
{
    public static void main( String[] args )
    {
        System.out.println( "Start adminApp" );
        SpringApplication.run(AdminApp.class, args);
    }
}
