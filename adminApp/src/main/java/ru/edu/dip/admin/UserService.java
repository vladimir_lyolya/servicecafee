package ru.edu.dip.admin;

import ru.edu.dip.admin.entity.UserInfo;

public interface UserService {

    public InfoToPage getRegistrationForm ();
    public InfoToPage getRegistration (UserInfo userInfo);
    public InfoToPage getEnterWithCodeActivation (UserInfo userInfo, String codeActivation,
                                                  String codeActivationFromUser);
    public InfoToPage getNewActivationCode(UserInfo userInfo);
    public InfoToPage getChangeEmail(UserInfo userInfo);
    public InfoToPage getEntry(UserInfo userInfo);
    public InfoToPage getPassword (UserInfo userInfo);
    public InfoToPage getEnterAfterGetPassword (UserInfo userInfo);

    public InfoToPage getMenuOrder (InfoToPage infoFromPage);
    public InfoToPage getMenuDelItem (InfoToPage infoFromPage);
    public InfoToPage getOrderEscape(InfoToPage infoFromPage);
    public InfoToPage getOrderStart(InfoToPage infoFromPage);
    public InfoToPage getOrderOut(InfoToPage infoFromPage);
    public InfoToPage getSendOrderToMail(InfoToPage infoFromPage);
    public InfoToPage getTableOrder(InfoToPage infoFromPage);
    public InfoToPage resultBookingTable(InfoToPage infoFromPage);
}
