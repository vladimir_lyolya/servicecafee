package ru.edu.dip.admin;

import ru.edu.dip.admin.entity.*;
import ru.edu.dip.admin.entity.Employees;

import java.util.List;
import java.util.UUID;

public interface UserRepository {

    UserInfo getUserByPhone(String phone);
    UserInfo getUserByEmail(String email);
    UserInfo addUser(UserInfo userInfo);

    List<Meals> getMeals();
    void setMeals(List<Meals> listMeals);

    Integer addUserMealsOrder(UserMealsOrder order);
    UserMealsOrder getUserMealsOrder(Integer key);
    List<UserMealsOrder> getUserMealsOrder(UUID userUUID);
    List<UserMealsOrder> getUserMealsOrderAll();

    DietTable getTable(UUID userUuid);
    void saveTable(DietTable dietTable);
    List<DietTable> getTables();

    void addToBookingList(Booking booking);
    List<Booking> getBookingList();
    void removeBookingTable(int numberOfTable);
    void deleteMeals(Long id);
    void saveMeals(Meals meals);

    void updateTableStatus(int numberOfTable);

    void updateBookingStatus(int id);
    void setEmployees(Employees employees);
    List<Employees> getEmployeesRole(String role);
    Employees getEmployeesLogin(String login);

}
