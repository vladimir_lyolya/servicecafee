package ru.edu.dip.admin.entity;

import javax.persistence.*;

@Entity
@Table(name="booking_tb")
public class Booking {

    @Id
    @Column(name="id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String bookingTableTime;
    private int numberOfTable;
    private String bookingStatus;
    private String bookingPerson;
    private String namePerson;
    @Transient
    private String telephoneNumber;


    public String getTelephoneNumber() {
        return telephoneNumber;
    }

    public void setTelephoneNumber(String telephoneNumber) {
        this.telephoneNumber = telephoneNumber;
    }
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBookingTableTime() {
        return bookingTableTime;
    }

    public void setBookingTableTime(String bookingTableTime) {
        this.bookingTableTime = bookingTableTime;
    }

    public int getNumberOfTable() {
        return numberOfTable;
    }

    public void setNumberOfTable(int numberOfTable) {
        this.numberOfTable = numberOfTable;
    }

    public String getBookingStatus() {
        return bookingStatus;
    }

    public void setBookingStatus(String bookingStatus) {
        this.bookingStatus = bookingStatus;
    }

    public String getBookingPerson() {
        return bookingPerson;
    }

    public void setBookingPerson(String bookingPerson) {
        this.bookingPerson = bookingPerson;
    }

    public String getNamePerson() {
        return namePerson;
    }

    public void setNamePerson(String namePerson) {
        this.namePerson = namePerson;
    }

    @Override
    public String toString() {
        return "Booking{" +
                "id=" + id +
                ", bookingTableTime='" + bookingTableTime + '\'' +
                ", numberOfTable=" + numberOfTable +
                ", bookingStatus='" + bookingStatus + '\'' +
                ", bookingPerson='" + bookingPerson + '\'' +
                ", namePerson='" + namePerson + '\'' +
                '}';
    }
}
