package ru.edu.dip.admin.entity;

import java.util.List;
import java.util.UUID;

public class UserMealsOrder {

    private Integer orderNumber;
    private String state;
    private Integer tableNumber;
    private String userName;
    private UUID userUUID;
    private long orderTime;
    private double totalPrice;
    private List<Meals> orderMeals;

    public UserMealsOrder() {
    }

    public UserMealsOrder(Integer orderNumber, String state, Integer tableNumber, String userName, UUID userUUID, long orderTime, double totalPrice, List<Meals> orderMeals) {
        this.orderNumber = orderNumber;
        this.state = state;
        this.tableNumber = tableNumber;
        this.userName = userName;
        this.userUUID = userUUID;
        this.orderTime = orderTime;
        this.totalPrice = totalPrice;
        this.orderMeals = orderMeals;
    }

    public List<Meals> getOrderMeals() {
        return orderMeals;
    }

    public void setOrderMeals(List<Meals> orderMeals) {
        this.orderMeals = orderMeals;
    }

    public Integer getTableNumber() {
        return tableNumber;
    }

    public void setTableNumber(Integer tableNumber) {
        this.tableNumber = tableNumber;
    }

    public UUID getUserUUID() {
        return userUUID;
    }

    public void setUserUUID(UUID userUUID) {
        this.userUUID = userUUID;
    }

    public long getOrderTime() {
        return orderTime;
    }

    public void setOrderTime(long orderTime) {
        this.orderTime = orderTime;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Integer getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(Integer orderNumber) {
        this.orderNumber = orderNumber;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    @Override
    public String toString() {
        return "UserMealsOrder{" +
                "orderMeals=" + orderMeals +
                ", orderNumber=" + orderNumber +
                ", state='" + state + '\'' +
                ", tableNumber=" + tableNumber +
                ", userName='" + userName + '\'' +
                ", userUUID=" + userUUID +
                ", orderTime=" + orderTime +
                ", totalPrice=" + totalPrice +
                '}';
    }
}
