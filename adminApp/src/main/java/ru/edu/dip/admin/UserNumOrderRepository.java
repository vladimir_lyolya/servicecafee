package ru.edu.dip.admin;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.edu.dip.admin.entity.NumberOrder;

@Repository
public interface UserNumOrderRepository extends CrudRepository<NumberOrder, Integer> {

}
