package ru.edu.dip.admin.convertor;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.core.convert.converter.Converter;
import ru.edu.dip.admin.entity.Meals;

public class MealsToString implements Converter<Meals, String> {
    @Override
    public String convert(Meals meals) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            String json = mapper.writeValueAsString(meals);
            return json;
        } catch (JsonProcessingException e) {
            System.out.println("StringToMeals converted failed!" + e.getMessage());
            return "new Meals()";
        }
    }
}
