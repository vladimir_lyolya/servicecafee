package ru.edu.dip.admin;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.edu.dip.admin.entity.Meals;

@Repository
public interface UserMealsRepository extends CrudRepository<Meals,Long> {

}
