package ru.edu.dip.admin.common;

import java.time.Instant;

public class TimeService {

    public static long getSecond(long offset){
        Instant timestamp = Instant.now();
        long second = timestamp.getEpochSecond() + offset;
        return second;
    }
}
