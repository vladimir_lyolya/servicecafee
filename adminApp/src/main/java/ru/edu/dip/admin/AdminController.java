package ru.edu.dip.admin;

import org.springframework.web.servlet.ModelAndView;
import ru.edu.dip.admin.entity.Booking;


public interface AdminController {

    public ModelAndView menu();
    public ModelAndView confirm();
    public ModelAndView redact();
    public ModelAndView afterChoiceTime();
    public ModelAndView resultBookingTable(Booking booking);
    public ModelAndView addDish();
    public ModelAndView removeDish();

}