package ru.edu.dip.admin;

public enum ListUserUI {
    index,
    userErrorPage,
    loginWithEmail,
    activationCodePage,
    registration,
    userNotFound,
    wrongPassword,
    passwordSendToEmail,
    reservationAndOrder,
    menuAndOrder,
    bookingTables,
    bookingResult,
    orderMealsFinal;

    public static String getURN(InfoToPage infoToPage) {
        if (infoToPage == null) return "redirect:/userErrorPage";
        switch (infoToPage.getUrnPage()){
            case index:
            case loginWithEmail:
            case userNotFound:
            case wrongPassword:
            case passwordSendToEmail: return "redirect:/login";
            case registration:
            case activationCodePage: return "redirect:/registration";
            case reservationAndOrder:
            case orderMealsFinal:
            case bookingTables:
            case bookingResult:
            case menuAndOrder: return "redirect:/booking";
            case userErrorPage:
            default: return "redirect:/error";
        }
    }
}
