package ru.edu.dip.cook;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CookApp
{
    public static void main( String[] args )
    {
        System.out.println( "Start cookApp" );
        SpringApplication.run(CookApp.class, args);
    }
}
