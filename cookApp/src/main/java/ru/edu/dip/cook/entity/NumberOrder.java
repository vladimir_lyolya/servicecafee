package ru.edu.dip.cook.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "number_order_tb")
public class NumberOrder {

    @Id
    @Column(name="id", nullable = false)
    private Integer id;
    private Integer numberOrder;

    public NumberOrder() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getNumberOrder() {
        return numberOrder;
    }

    public void setNumberOrder(Integer numberOrder) {
        this.numberOrder = numberOrder;
    }

    @Override
    public String toString() {
        return "NumberOrder{" +
                "id=" + id +
                ", numberOrder=" + numberOrder +
                '}';
    }
}
