package ru.edu.dip.cook;

import ru.edu.dip.cook.entity.Booking;
import ru.edu.dip.cook.entity.DietTable;
import ru.edu.dip.cook.entity.MealsMenu;
import ru.edu.dip.cook.entity.UserInfo;
import ru.edu.dip.cook.entity.UserMealsOrder;

import java.util.List;
import java.util.Set;

public class InfoToPage {

    private ListUserUI urnPage;
    private String message;
    private String codeActivation;
    private String codeActivationFromUser;
    private UserInfo userInfo;
    private MealsMenu mealsMenu;
    private UserMealsOrder userMealsOrder;
    private List<UserMealsOrder> userMealsOrderReady;
    private List<UserMealsOrder> userMealsOrderCompleted;
    private int selectOrder;

    private List<DietTable> tables;
    private Set<String> allTime;
    private Booking booking;
    private int selectTable;

    public int getSelectOrder() {
        return selectOrder;
    }

    public void setSelectOrder(int selectOrder) {
        this.selectOrder = selectOrder;
    }

    public List<UserMealsOrder> getUserMealsOrderReady() {
        return userMealsOrderReady;
    }

    public void setUserMealsOrderReady(List<UserMealsOrder> userMealsOrderReady) {
        this.userMealsOrderReady = userMealsOrderReady;
    }

    public List<UserMealsOrder> getUserMealsOrderCompleted() {
        return userMealsOrderCompleted;
    }

    public void setUserMealsOrderCompleted(List<UserMealsOrder> userMealsOrderCompleted) {
        this.userMealsOrderCompleted = userMealsOrderCompleted;
    }

    public int getSelectTable() {
        return selectTable;
    }

    public void setSelectTable(int selectTable) {
        this.selectTable = selectTable;
    }

    public Booking getBooking() {
        return booking;
    }

    public void setBooking(Booking booking) {
        this.booking = booking;
    }

    public List<DietTable> getTables() {
        return tables;
    }

    public void setTables(List<DietTable> tables) {
        this.tables = tables;
    }

    public Set<String> getAllTime() {
        return allTime;
    }

    public void setAllTime(Set<String> allTime) {
        this.allTime = allTime;
    }

    public InfoToPage(){

    }

    public InfoToPage(ListUserUI urnPage, UserInfo userInfo,
                      String message, String codeActivation) {
        this.urnPage = urnPage;
        this.userInfo = userInfo;
        this.message = message;
        this.codeActivation = codeActivation;
    }

    public ListUserUI getUrnPage() {
        return urnPage;
    }

    public void setUrnPage(ListUserUI urnPage) {
        this.urnPage = urnPage;
    }

    public UserInfo getUserInfo() {
        return userInfo;
    }

    public void setUserInfo(UserInfo userInfo) {
        this.userInfo = userInfo;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCodeActivation() {
        return codeActivation;
    }

    public void setCodeActivation(String codeActivation) {
        this.codeActivation = codeActivation;
    }

    public String getCodeActivationFromUser() {
        return codeActivationFromUser;
    }

    public void setCodeActivationFromUser(String codeActivationFromUser) {
        this.codeActivationFromUser = codeActivationFromUser;
    }

    public MealsMenu getMealsMenu() {
        return mealsMenu;
    }

    public void setMealsMenu(MealsMenu mealsMenu) {
        this.mealsMenu = mealsMenu;
    }

    public UserMealsOrder getUserMealsOrder() {
        return userMealsOrder;
    }

    public void setUserMealsOrder(UserMealsOrder userMealsOrder) {
        this.userMealsOrder = userMealsOrder;
    }
}
