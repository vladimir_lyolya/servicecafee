package ru.edu.dip.cook.impl;

import org.springframework.boot.autoconfigure.web.servlet.error.ErrorViewResolver;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@Controller
public class CookErrorViewResolver implements ErrorViewResolver {
    @Override
    public ModelAndView resolveErrorView(HttpServletRequest request, HttpStatus status, Map<String, Object> model) {
        String statusPage = status.toString();
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("statusPage", statusPage);
        modelAndView.setViewName("userErrorPage");
        return modelAndView;
    }
}
