package ru.edu.dip.cook.config;

import javax.annotation.PostConstruct;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.context.SecurityContextHolder;

@Configuration
public class SecurityConfig {

    @PostConstruct
    public void enableAuthCtxOnSpawnedThreads() {
        System.out.println("___________________________________________________________");
        System.out.println(SecurityContextHolder.getContextHolderStrategy().toString());
        SecurityContextHolder.setStrategyName(SecurityContextHolder.MODE_INHERITABLETHREADLOCAL);
        System.out.println("***********************************************************");
        System.out.println(SecurityContextHolder.getContextHolderStrategy().toString());
    }
}
