package ru.edu.dip.cook;

public enum ListUserUI {
    index,
    index_cook,
    select_table,
    userErrorPage,
    loginWithEmail,
    activationCodePage,
    registration,
    userNotFound,
    wrongPassword,
    passwordSendToEmail,
    reservationAndOrder,
    menuAndOrder,
    bookingTables,
    bookingResult,
    orderMealsFinal;

    public static String getURN(InfoToPage infoToPage) {
        if (infoToPage == null) return "redirect:/userErrorPage";
        switch (infoToPage.getUrnPage()){
            case index:
            case index_cook:
            case select_table:
            case loginWithEmail:
            case userNotFound:
            case wrongPassword:
            case passwordSendToEmail: return "redirect:/cook";
            case registration:
            case activationCodePage: return "redirect:/registration";
            case reservationAndOrder:
            case orderMealsFinal:
            case bookingTables:
            case bookingResult:
            case menuAndOrder: return "redirect:/booking";
            case userErrorPage:
            default: return "redirect:/error";
        }
    }
}
