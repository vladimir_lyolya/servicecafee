package ru.edu.dip.cook.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import ru.edu.dip.cook.CookService;
import ru.edu.dip.cook.InfoToPage;
import ru.edu.dip.cook.ListUserUI;
import ru.edu.dip.cook.entity.Employees;

@Controller
@SessionAttributes(value = "infoToPage", types= InfoToPage.class)
public class CookControllerImpl {

    private CookService cookService;

    @Autowired
    public CookControllerImpl(CookService cookService){
        this.cookService = cookService;
    }

    @GetMapping(value="/cook")
    public String getLoginURN(@ModelAttribute("infoToPage") InfoToPage infoToPage) {
        return infoToPage.getUrnPage().toString();
    }

    @GetMapping(value="/")
    public String index(Model model, RedirectAttributes redirectAttrs){
        Employees employees = cookService.getEmployees();
        InfoToPage infoToPage = new InfoToPage();
        infoToPage.setMessage(employees.getFirstName() + ", Login : " + employees.getLogin());
        infoToPage.setUrnPage(ListUserUI.index);
        model.addAttribute("infoToPage", infoToPage);
        redirectAttrs.addFlashAttribute("infoToPage", infoToPage);
        return ListUserUI.getURN(infoToPage);
    }

    @PostMapping(value = "/getUpdateListOrder")
    public String getUpdateListOrder(@ModelAttribute("infoToPage") InfoToPage infoFromPage,
                                     RedirectAttributes redirectAttrs) {
        InfoToPage infoToPage = cookService.getUpdateListOrder(infoFromPage);
        redirectAttrs.addFlashAttribute("infoToPage", infoToPage);
        return ListUserUI.getURN(infoToPage);
    }

    @PostMapping(value = "/getSetCompleted")
    public String getSetCompleted(@ModelAttribute("infoToPage") InfoToPage infoFromPage,
                                  RedirectAttributes redirectAttrs) {
        InfoToPage infoToPage = cookService.getSetCompleted(infoFromPage);
        redirectAttrs.addFlashAttribute("infoToPage", infoToPage);
        return ListUserUI.getURN(infoToPage);
    }


    @PostMapping(value = "/getCookStart")
    public String getWaiterStart(@ModelAttribute("infoToPage") InfoToPage infoFromPage,
                                 RedirectAttributes redirectAttrs) {
        infoFromPage.setUrnPage(ListUserUI.index_cook);
        redirectAttrs.addFlashAttribute("infoToPage", infoFromPage);
        return ListUserUI.getURN(infoFromPage);
    }

    @RequestMapping("/login")
    public String login() {
        return "login";
    }


}