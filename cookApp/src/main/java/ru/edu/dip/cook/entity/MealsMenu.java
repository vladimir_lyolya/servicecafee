package ru.edu.dip.cook.entity;

import java.util.ArrayList;
import java.util.List;

public class MealsMenu {

    private List<Meals> soupMeals = new ArrayList<>();
    private List<Meals> mainMeals = new ArrayList<>();
    private List<Meals> drinkMeals = new ArrayList<>();

    private Meals userChoice;

    public MealsMenu() {
    }

    public List<Meals> getSoupMeals() {
        return soupMeals;
    }

    public void setSoupMeals(List<Meals> soupMeals) {
        this.soupMeals = soupMeals;
    }

    public List<Meals> getMainMeals() {
        return mainMeals;
    }

    public void setMainMeals(List<Meals> mainMeals) {
        this.mainMeals = mainMeals;
    }

    public List<Meals> getDrinkMeals() {
        return drinkMeals;
    }

    public void setDrinkMeals(List<Meals> drinkMeals) {
        this.drinkMeals = drinkMeals;
    }

    public Meals getUserChoice() {
        return userChoice;
    }

    public void setUserChoice(Meals userChoice) {
        this.userChoice = userChoice;
    }

    @Override
    public String toString() {
        return "MealsMenu{" +
                "soupMeals=" + soupMeals +
                ", mainMeals=" + mainMeals +
                ", drinkMeals=" + drinkMeals +
                ", userChoice='" + userChoice + '\'' +
                '}';
    }
}
