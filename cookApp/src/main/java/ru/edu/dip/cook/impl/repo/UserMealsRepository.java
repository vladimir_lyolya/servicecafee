package ru.edu.dip.cook.impl.repo;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.edu.dip.cook.entity.Meals;

@Repository
public interface UserMealsRepository extends CrudRepository<Meals,Long> {

}
