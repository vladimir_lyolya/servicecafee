package ru.edu.dip.cook.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import ru.edu.dip.cook.CookService;
import ru.edu.dip.cook.InfoToPage;
import ru.edu.dip.cook.ListUserUI;
import ru.edu.dip.cook.UserRepository;
import ru.edu.dip.cook.entity.Employees;
import ru.edu.dip.cook.entity.Meals;
import ru.edu.dip.cook.entity.UserMealsOrder;

import java.util.List;

@Service
public class CookServiceImpl implements CookService {

    private UserRepository cookRepository;

    private Employees activeEmployees;

    @Autowired
    public CookServiceImpl(UserRepository cookRepository) {
        this.cookRepository = cookRepository;
    }

    @Override
    public List<Meals> getMealsFromRepo(){
        return cookRepository.getMeals();
    }

    @Override
    public List<UserMealsOrder> getOrderList(){
        return cookRepository.getOrdersFromUser();
    }

    @Override
    public void updateMealsInOrder(UserMealsOrder order) {

    }

    @Override
    public void addToReadyOrderList(UserMealsOrder order) {
        cookRepository.addToReadyOrderList(order);
    }

    @Override
    public List<UserMealsOrder> getReadyOrderList() {
        return cookRepository.getReadyOrderList();
    }

    @Override
    public Employees getEmployees(){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String login = authentication.getName();
        Employees employees = cookRepository.getEmployeesLogin(login);
        activeEmployees = employees;
        return employees;
    }

    private InfoToPage getInfoToPageToView(InfoToPage infoToPage){
        InfoToPage tempInfo = new InfoToPage();
        tempInfo.setUrnPage(infoToPage.getUrnPage());
        tempInfo.setMessage(infoToPage.getMessage());
        tempInfo.setCodeActivation(infoToPage.getCodeActivation());
        tempInfo.setCodeActivationFromUser(infoToPage.getCodeActivationFromUser());
        tempInfo.setUserInfo(infoToPage.getUserInfo());
        tempInfo.setMealsMenu(infoToPage.getMealsMenu());
        tempInfo.setUserMealsOrder(infoToPage.getUserMealsOrder());
        tempInfo.setTables(infoToPage.getTables());
        tempInfo.setAllTime(infoToPage.getAllTime());
        tempInfo.setBooking(infoToPage.getBooking());
        tempInfo.setSelectTable(infoToPage.getSelectTable());
        tempInfo.setSelectOrder(infoToPage.getSelectOrder());
        tempInfo.setUserMealsOrderReady((infoToPage.getUserMealsOrderReady()));
        tempInfo.setUserMealsOrderCompleted(infoToPage.getUserMealsOrderCompleted());
        return tempInfo;
    }

    @Override
    public InfoToPage getUpdateListOrder(InfoToPage infoFromPage) {
        InfoToPage infoToPage = getInfoToPageToView(infoFromPage);
        infoToPage.setUrnPage(ListUserUI.index_cook);
        List<UserMealsOrder> userMealsOrderReady = cookRepository.getUserMealsOrderByState("Заказан");
        infoToPage.setUserMealsOrderReady(userMealsOrderReady);
        List<UserMealsOrder> userMealsOrderCompleted = cookRepository.getUserMealsOrderByState("Готов");
        infoToPage.setUserMealsOrderCompleted(userMealsOrderCompleted);
        return infoToPage;
    }

    @Override
    public InfoToPage getSetCompleted(InfoToPage infoFromPage) {
        InfoToPage infoToPage = getInfoToPageToView(infoFromPage);
        infoToPage.setUrnPage(ListUserUI.index_cook);
        Integer selectOrder = infoToPage.getSelectOrder();
        List<UserMealsOrder> listOrdersReady = infoToPage.getUserMealsOrderReady();
        UserMealsOrder tmpOrder = new UserMealsOrder();
        boolean flag = false;
        for (UserMealsOrder order: listOrdersReady) {
            if (order.getOrderNumber().equals(selectOrder)){
                order.setState("Готов");
                cookRepository.saveUserMealsOrder(order);
                flag = true;
                tmpOrder = order;
            }
        }
        if (flag) listOrdersReady.remove(tmpOrder);
        infoToPage.setUserMealsOrderReady(listOrdersReady);
        List<UserMealsOrder> userMealsOrderCompleted = cookRepository.getUserMealsOrderByState("Готов");
        infoToPage.setUserMealsOrderCompleted(userMealsOrderCompleted);
        return infoToPage;
    }

}

