package ru.edu.dip.cook.impl.repo;

import org.springframework.data.repository.CrudRepository;
import ru.edu.dip.cook.entity.Employees;

import java.util.ArrayList;

public interface UserEmployeesRepository extends CrudRepository<Employees, Long> {
    ArrayList<Employees> findByLogin(String login);
    ArrayList<Employees> findByRole(String role);
}
