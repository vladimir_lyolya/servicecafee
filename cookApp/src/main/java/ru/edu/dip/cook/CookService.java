package ru.edu.dip.cook;

import ru.edu.dip.cook.entity.Employees;
import ru.edu.dip.cook.entity.Meals;
import ru.edu.dip.cook.entity.UserMealsOrder;

import java.util.List;

public interface CookService {

    public List<Meals> getMealsFromRepo();
    public List<UserMealsOrder> getOrderList();
    public void updateMealsInOrder(UserMealsOrder order);
    public void addToReadyOrderList(UserMealsOrder order);
    public List<UserMealsOrder> getReadyOrderList();
    Employees getEmployees();
    InfoToPage getUpdateListOrder (InfoToPage infoFromPage);
    InfoToPage getSetCompleted(InfoToPage infoFromPage);
}
