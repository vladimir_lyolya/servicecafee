package ru.edu.dip.cook.impl.repo;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.edu.dip.cook.entity.Booking;

import java.util.ArrayList;

@Repository
public interface UserBookingRepository extends CrudRepository<Booking, Long> {
    ArrayList<Booking> findByNumberOfTable(int numberOfTable);
}
