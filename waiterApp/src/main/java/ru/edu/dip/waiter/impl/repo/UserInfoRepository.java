package ru.edu.dip.waiter.impl.repo;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.edu.dip.waiter.entity.UserInfo;

import java.util.ArrayList;

@Repository
public interface UserInfoRepository extends CrudRepository<UserInfo, Long> {

    ArrayList<UserInfo> findByPhone(String phone);

    ArrayList<UserInfo> findByEmail(String email);
}
