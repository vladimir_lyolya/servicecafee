package ru.edu.dip.waiter;

import ru.edu.dip.waiter.entity.Employees;
import ru.edu.dip.waiter.entity.UserInfo;

public interface UserService {

    InfoToPage getRegistrationForm();
    InfoToPage getRegistration (UserInfo userInfo);
    InfoToPage getEnterWithCodeActivation (UserInfo userInfo, String codeActivation,
                                                  String codeActivationFromUser);
    InfoToPage getNewActivationCode(UserInfo userInfo);
    InfoToPage getChangeEmail(UserInfo userInfo);
    InfoToPage getEntry(UserInfo userInfo);
    InfoToPage getPassword (UserInfo userInfo);
    InfoToPage getEnterAfterGetPassword (UserInfo userInfo);

    InfoToPage geMakeOrder (InfoToPage infoFromPage);
    InfoToPage getMenuOrder (InfoToPage infoFromPage);
    InfoToPage getUpdateListOrder (InfoToPage infoFromPage);
    InfoToPage getSetCompleted(InfoToPage infoFromPage);

    InfoToPage getMenuDelItem (InfoToPage infoFromPage);
    InfoToPage getOrderEscape(InfoToPage infoFromPage);
    InfoToPage getOrderStart(InfoToPage infoFromPage);
    InfoToPage getOrderOut(InfoToPage infoFromPage);
    InfoToPage getSendOrderToMail(InfoToPage infoFromPage);
    InfoToPage getTableOrder(InfoToPage infoFromPage);
    InfoToPage resultBookingTable(InfoToPage infoFromPage);
    InfoToPage getOrderWaiter(InfoToPage infoFromPage);
    Employees getEmployees();
}
