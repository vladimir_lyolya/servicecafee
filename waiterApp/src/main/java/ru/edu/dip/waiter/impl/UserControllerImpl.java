package ru.edu.dip.waiter.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import ru.edu.dip.waiter.InfoToPage;
import ru.edu.dip.waiter.ListUserUI;
import ru.edu.dip.waiter.UserController;
import ru.edu.dip.waiter.UserService;
import ru.edu.dip.waiter.entity.Employees;

@Controller
@SessionAttributes(value = "infoToPage", types= InfoToPage.class)
public class UserControllerImpl implements UserController {

    private UserService userService;

    @Autowired
    public UserControllerImpl(UserService userService) {
        this.userService = userService;
    }

    @GetMapping(value="/waiter")
    public String getLoginURN(@ModelAttribute("infoToPage") InfoToPage infoToPage) {
        return infoToPage.getUrnPage().toString();
    }

    @GetMapping(value="/registration")
    public String getRegistrationURN(@ModelAttribute("infoToPage") InfoToPage infoToPage){
        return infoToPage.getUrnPage().toString();
    }

    @GetMapping(value="/booking")
    public String getBookingURN(@ModelAttribute("infoToPage") InfoToPage infoToPage){
        return infoToPage.getUrnPage().toString();
    }

    @GetMapping(value="/error")
    public String getErrorURN(@ModelAttribute("infoToPage") InfoToPage infoToPage){
        return infoToPage.getUrnPage().toString();
    }

    @Override
    @PostMapping(value="/getRegistrationForm")
    public String getRegistrationForm(RedirectAttributes redirectAttrs) {
        InfoToPage infoToPage = userService.getRegistrationForm();
        redirectAttrs.addFlashAttribute("infoToPage", infoToPage);
        return ListUserUI.getURN(infoToPage);
    }

    @Override
    @PostMapping(value="/getRegistration")
    public String getRegistration(@ModelAttribute("infoToPage") InfoToPage infoFromPage,
                                  RedirectAttributes redirectAttrs) {
        InfoToPage infoToPage = userService.getRegistration(infoFromPage.getUserInfo());
        redirectAttrs.addFlashAttribute("infoToPage", infoToPage);
        return ListUserUI.getURN(infoToPage);
    }

    @Override
    @PostMapping(value="/getEnterWithCodeActivation")
    public String getEnterWithCodeActivation(@ModelAttribute("infoToPage") InfoToPage infoFromPage,
                                                 RedirectAttributes redirectAttrs) {
        InfoToPage infoToPage = userService.getEnterWithCodeActivation(infoFromPage.getUserInfo(),
                infoFromPage.getCodeActivation(), infoFromPage.getCodeActivationFromUser());
        redirectAttrs.addFlashAttribute("infoToPage", infoToPage);
        return ListUserUI.getURN(infoToPage);
    }

    @Override
    @PostMapping(value="/getNewActivationCode")
    public String getNewActivationCode(@ModelAttribute("infoToPage") InfoToPage infoFromPage,
                                           RedirectAttributes redirectAttrs) {
        InfoToPage infoToPage = userService.getNewActivationCode(infoFromPage.getUserInfo());
        redirectAttrs.addFlashAttribute("infoToPage", infoToPage);
        return ListUserUI.getURN(infoToPage);
    }

    @Override
    @PostMapping(value="/getChangeEmail")
    public String getChangeEmail(@ModelAttribute("infoToPage") InfoToPage infoFromPage,
                                     RedirectAttributes redirectAttrs) {
        InfoToPage infoToPage = userService.getChangeEmail(infoFromPage.getUserInfo());
        redirectAttrs.addFlashAttribute("infoToPage", infoToPage);
        return ListUserUI.getURN(infoToPage);
    }

    @Override
    @PostMapping(value="/getEntry")
    public String getEntry(@ModelAttribute("infoToPage") InfoToPage infoFromPage,
                                 RedirectAttributes redirectAttrs) {
        InfoToPage infoToPage = userService.getEntry(infoFromPage.getUserInfo());
        redirectAttrs.addFlashAttribute("infoToPage", infoToPage);
        return ListUserUI.getURN(infoToPage);
    }

    @Override
    @PostMapping(value="/getPassword")
    public String getPassword(@ModelAttribute("infoToPage") InfoToPage infoFromPage,
                              RedirectAttributes redirectAttrs) {
        InfoToPage infoToPage = userService.getPassword(infoFromPage.getUserInfo());
        redirectAttrs.addFlashAttribute("infoToPage", infoToPage);
        return ListUserUI.getURN(infoToPage);
    }

    @Override
    @PostMapping(value="/getEnterAfterGetPassword")
    public String getEnterAfterGetPassword(@ModelAttribute("infoToPage") InfoToPage infoFromPage,
                                               RedirectAttributes redirectAttrs) {
        InfoToPage infoToPage = userService.getEnterAfterGetPassword(infoFromPage.getUserInfo());
        redirectAttrs.addFlashAttribute("infoToPage", infoToPage);
        return ListUserUI.getURN(infoToPage);
    }

    @PostMapping(value="/getMenuOrder")
    public String getMenuOrder(@ModelAttribute("infoToPage") InfoToPage infoFromPage,
                                 RedirectAttributes redirectAttrs) {
        InfoToPage infoToPage = userService.getMenuOrder(infoFromPage);
        redirectAttrs.addFlashAttribute("infoToPage", infoToPage);
        return ListUserUI.getURN(infoToPage);
    }

    @PostMapping(value="/getMenuDelItem")
    public String getMenuDelItem(@ModelAttribute("infoToPage") InfoToPage infoFromPage,
                               RedirectAttributes redirectAttrs) {
        InfoToPage infoToPage = userService.getMenuDelItem(infoFromPage);
        redirectAttrs.addFlashAttribute("infoToPage", infoToPage);
        return ListUserUI.getURN(infoToPage);
    }

    @PostMapping(value="/getOrderEscape")
    public String getOrderEscape(@ModelAttribute("infoToPage") InfoToPage infoFromPage,
                                 RedirectAttributes redirectAttrs) {
        InfoToPage infoToPage = userService.getOrderEscape(infoFromPage);
        redirectAttrs.addFlashAttribute("infoToPage", infoToPage);
        return ListUserUI.getURN(infoToPage);
    }


    @PostMapping(value="/getOrderOut")
    public String getOrderOut(@ModelAttribute("infoToPage") InfoToPage infoFromPage,
                                RedirectAttributes redirectAttrs) {
        InfoToPage infoToPage = userService.getOrderOut(infoFromPage);
        redirectAttrs.addFlashAttribute("infoToPage", infoToPage);
        return ListUserUI.getURN(infoToPage);
    }

    @PostMapping(value="/getOrderStart")
    public String getOrderStart(@ModelAttribute("infoToPage") InfoToPage infoFromPage,
                                 RedirectAttributes redirectAttrs) {
        InfoToPage infoToPage = userService.getOrderStart(infoFromPage);
        redirectAttrs.addFlashAttribute("infoToPage", infoToPage);
        return ListUserUI.getURN(infoToPage);
    }

    @PostMapping(value="/getSendOrderToMail")
    public String getSendOrderToMail(@ModelAttribute("infoToPage") InfoToPage infoFromPage,
                                RedirectAttributes redirectAttrs) {
        InfoToPage infoToPage = userService.getSendOrderToMail(infoFromPage);
        redirectAttrs.addFlashAttribute("infoToPage", infoToPage);
        return ListUserUI.getURN(infoToPage);
    }


    @PostMapping(value="/getTableOrder")
    public String getTableOrder(@ModelAttribute("infoToPage") InfoToPage infoFromPage,
                                     RedirectAttributes redirectAttrs) {
        InfoToPage infoToPage = userService.getTableOrder(infoFromPage);
        redirectAttrs.addFlashAttribute("infoToPage", infoToPage);
        return ListUserUI.getURN(infoToPage);
    }

    @PostMapping(value="/resultBookingTable")
    public String resultBookingTable(@ModelAttribute("infoToPage") InfoToPage infoFromPage,
                                RedirectAttributes redirectAttrs) {
        InfoToPage infoToPage = userService.resultBookingTable(infoFromPage);
        redirectAttrs.addFlashAttribute("infoToPage", infoToPage);
        return ListUserUI.getURN(infoToPage);
    }

    @PostMapping(value = "/geMakeOrder")
    public String geMakeOrder(@ModelAttribute("infoToPage") InfoToPage infoFromPage,
                               RedirectAttributes redirectAttrs) {
        InfoToPage infoToPage = userService.geMakeOrder(infoFromPage);
        redirectAttrs.addFlashAttribute("infoToPage", infoToPage);
        return ListUserUI.getURN(infoToPage);
    }

    @PostMapping(value = "/getOrderWaiter")
    public String getOrderWaiter(@ModelAttribute("infoToPage") InfoToPage infoFromPage,
                              RedirectAttributes redirectAttrs) {
        InfoToPage infoToPage = userService.getOrderWaiter(infoFromPage);
        redirectAttrs.addFlashAttribute("infoToPage", infoToPage);
        return ListUserUI.getURN(infoToPage);
    }


    @PostMapping(value = "/getUpdateListOrder")
    public String getUpdateListOrder(@ModelAttribute("infoToPage") InfoToPage infoFromPage,
                              RedirectAttributes redirectAttrs) {
        InfoToPage infoToPage = userService.getUpdateListOrder(infoFromPage);
        redirectAttrs.addFlashAttribute("infoToPage", infoToPage);
        return ListUserUI.getURN(infoToPage);
    }

    @PostMapping(value = "/getSetCompleted")
    public String getSetCompleted(@ModelAttribute("infoToPage") InfoToPage infoFromPage,
                                     RedirectAttributes redirectAttrs) {
        InfoToPage infoToPage = userService.getSetCompleted(infoFromPage);
        redirectAttrs.addFlashAttribute("infoToPage", infoToPage);
        return ListUserUI.getURN(infoToPage);
    }


    @PostMapping(value = "/getWaiterStart")
    public String getWaiterStart(@ModelAttribute("infoToPage") InfoToPage infoFromPage,
                                     RedirectAttributes redirectAttrs) {
        infoFromPage.setUrnPage(ListUserUI.index_waiter);
        redirectAttrs.addFlashAttribute("infoToPage", infoFromPage);
        return ListUserUI.getURN(infoFromPage);
    }

    @GetMapping(value="/")
    public String index(Model model, RedirectAttributes redirectAttrs){
        Employees employees = userService.getEmployees();
        InfoToPage infoToPage = new InfoToPage();
        infoToPage.setMessage(employees.getFirstName() + ", Login : " + employees.getLogin());
        infoToPage.setUrnPage(ListUserUI.index);
        model.addAttribute("infoToPage", infoToPage);
        redirectAttrs.addFlashAttribute("infoToPage", infoToPage);
        return ListUserUI.getURN(infoToPage);
    }

    @RequestMapping("/login")
    public String login() {
        return "login";
    }
}
