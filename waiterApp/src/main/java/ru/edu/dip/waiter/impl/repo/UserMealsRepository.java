package ru.edu.dip.waiter.impl.repo;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.edu.dip.waiter.entity.Meals;

@Repository
public interface UserMealsRepository extends CrudRepository<Meals,Long> {

}
