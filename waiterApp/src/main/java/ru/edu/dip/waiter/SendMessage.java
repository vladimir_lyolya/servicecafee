package ru.edu.dip.waiter;

public interface SendMessage {

    public String sendMessage(String message);
    public void setDestination(String destination);
}
