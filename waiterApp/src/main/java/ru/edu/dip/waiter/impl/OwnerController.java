package ru.edu.dip.waiter.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import ru.edu.dip.waiter.UserRepository;
import ru.edu.dip.waiter.entity.DietTable;
import ru.edu.dip.waiter.entity.Employees;
import ru.edu.dip.waiter.entity.Meals;
import ru.edu.dip.waiter.entity.UserInfo;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Controller
public class OwnerController {

    private UserRepository userRepository;

    @Autowired
    public void setUserRepository(UserRepository userRepository){
        this.userRepository = userRepository;
    }

    @PostConstruct
    public void init(){
        if (userRepository.getMeals().size() == 0) {
            UUID uuid = UUID.randomUUID();
            UserInfo userInfo = new UserInfo("Name1", "LastName1",
                    "Name1@mail.ru", "+79604641374", uuid, "1234");
            userRepository.addUser(userInfo);

            uuid = UUID.randomUUID();
            userInfo = new UserInfo("Name2", "LastName2",
                    "Name2@mail.ru", "+79604641374", uuid, "4321");
            userRepository.addUser(userInfo);

            List<Meals> listMeals = new ArrayList<>();
            Meals meals = new Meals();
            meals.setType("soup");
            meals.setTitle("Куриный бульон");
            meals.setPrice("100");
            meals.setDescription("Очень вкусный куриный бульон");
            meals.setLinkImage("chickensoup.jpg");
            listMeals.add(meals);
            meals = new Meals();
            meals.setType("soup");
            meals.setTitle("Борщ");
            meals.setPrice("120");
            meals.setDescription("Очень вкусный борщ");
            meals.setLinkImage("borsch.jpg");
            listMeals.add(meals);
            meals = new Meals();
            meals.setType("soup");
            meals.setTitle("Харчо");
            meals.setPrice("130");
            meals.setDescription("Очень вкусный харчо");
            meals.setLinkImage("harcho.jpg");
            listMeals.add(meals);
            meals = new Meals();
            meals.setType("main");
            meals.setTitle("Жаркое из говядины");
            meals.setPrice("140");
            meals.setDescription("Очень вкусное жаркое из говядины");
            meals.setLinkImage("meal.jpg");
            listMeals.add(meals);
            meals = new Meals();
            meals.setType("main");
            meals.setTitle("Салат Оливье");
            meals.setPrice("150");
            meals.setDescription("Очень вкусный савлат оливье");
            meals.setLinkImage("salad.jpg");
            listMeals.add(meals);
            meals = new Meals();
            meals.setType("main");
            meals.setTitle("Свиная отбивная");
            meals.setPrice("160");
            meals.setDescription("Очень вкусная отбивная");
            meals.setLinkImage("porkchop.jpg");
            listMeals.add(meals);
            meals = new Meals();
            meals.setType("drink");
            meals.setTitle("Клюквенный морс");
            meals.setPrice("50");
            meals.setDescription("Очень вкусный морс");
            meals.setLinkImage("mors.jpg");
            listMeals.add(meals);
            meals = new Meals();
            meals.setType("drink");
            meals.setTitle("Ягодный Сок");
            meals.setPrice("60");
            meals.setDescription("Очень вкусный сок");
            meals.setLinkImage("berryjuice.jpg");
            listMeals.add(meals);
            meals = new Meals();
            meals.setType("drink");
            meals.setTitle("Зеленый чай");
            meals.setPrice("40");
            meals.setDescription("Очень вкусный чай");
            meals.setLinkImage("greentea.jpg");
            listMeals.add(meals);
            userRepository.setMeals(listMeals);

            DietTable dietTable = new DietTable();
            dietTable.setId(1);
            dietTable.setNumberTable(1);
            dietTable.setPlace(4);
            dietTable.setStatus(false);
            dietTable.setUser(UUID.randomUUID());
            dietTable.setBookingTime(0);
            dietTable.setUserName("");
            userRepository.saveTable(dietTable);
            dietTable.setId(2);
            dietTable.setNumberTable(2);
            userRepository.saveTable(dietTable);
            dietTable.setId(3);
            dietTable.setNumberTable(3);
            userRepository.saveTable(dietTable);
            dietTable.setId(4);
            dietTable.setNumberTable(4);
            userRepository.saveTable(dietTable);

            Employees employees = new Employees();
            employees.setLogin("waiter01");
            employees.setPassword(new BCryptPasswordEncoder().encode("waiter01"));
            employees.setFirstName("Светлана");
            employees.setLastName("Лапушкина");
            employees.setPhone("+7955-400-22-33");
            employees.setRole("waiter");
            employees.setOnWork(false);
            employees.setSchedule("Monday;Wednesday;Friday");
            employees.setEmployeesUUID(UUID.randomUUID());
            userRepository.setEmployees(employees);
            employees = new Employees();
            employees.setLogin("waiter02");
            employees.setPassword(new BCryptPasswordEncoder().encode("waiter02"));
            employees.setFirstName("Константин");
            employees.setLastName("Разговоров");
            employees.setPhone("+7955-400-22-44");
            employees.setRole("waiter");
            employees.setOnWork(false);
            employees.setSchedule("Tuesday;Thursday;Saturday");
            employees.setEmployeesUUID(UUID.randomUUID());
            userRepository.setEmployees(employees);

        }
    }

}
