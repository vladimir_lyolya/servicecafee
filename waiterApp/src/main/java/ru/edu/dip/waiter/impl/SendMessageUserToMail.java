package ru.edu.dip.waiter.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.mail.MailSendException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ru.edu.dip.waiter.SendMessage;

@Component
@Qualifier("SendMessageToUse")
@PropertySource(value = "classpath:userApp.config", encoding = "UTF-8")
public class SendMessageUserToMail implements SendMessage {

    private String destination;

    @Value("${subjectMessageToEmail.user}")
    private String subjectMessageToEmail;

    @Autowired
    private JavaMailSender javaMailSender;

    public SendMessageUserToMail(){

    }

    @Override
    @Async
    public String sendMessage(String message) {
        try {

            SimpleMailMessage msg = new SimpleMailMessage();
            msg.setTo(destination);

            msg.setSubject(subjectMessageToEmail);
            msg.setText(message);

            javaMailSender.send(msg);

        } catch (MailSendException ex) {
            return null;
        }
        return "ok";
    }

    @Override
    public void setDestination(String destination) {
        this.destination = destination;
    }
}
