package ru.edu.dip.waiter.impl.repo;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.edu.dip.waiter.entity.DietTable;

import java.util.ArrayList;
import java.util.UUID;

@Repository
public interface UserTableRepository extends CrudRepository<DietTable, Integer> {

    ArrayList<DietTable> findByUserUUID(UUID uuid);
}
