package ru.edu.dip.waiter.impl.repo;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.edu.dip.waiter.entity.NumberOrder;

@Repository
public interface UserNumOrderRepository extends CrudRepository<NumberOrder, Integer> {

}
