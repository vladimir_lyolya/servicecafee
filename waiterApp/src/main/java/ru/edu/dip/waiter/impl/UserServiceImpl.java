package ru.edu.dip.waiter.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.edu.dip.waiter.InfoToPage;
import ru.edu.dip.waiter.ListUserUI;
import ru.edu.dip.waiter.SendMessage;
import ru.edu.dip.waiter.UserRepository;
import ru.edu.dip.waiter.UserService;
import ru.edu.dip.waiter.common.TimeService;
import ru.edu.dip.waiter.entity.Booking;
import ru.edu.dip.waiter.entity.DietTable;
import ru.edu.dip.waiter.entity.Employees;
import ru.edu.dip.waiter.entity.Meals;
import ru.edu.dip.waiter.entity.MealsMenu;
import ru.edu.dip.waiter.entity.UserInfo;
import ru.edu.dip.waiter.entity.UserMealsOrder;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
@PropertySource(value = "classpath:userApp.config", encoding = "UTF-8")
public class UserServiceImpl implements UserService {

    private UserRepository userRepository;

    private SendMessage userSendMessage;

    private Employees activeEmployees;

    public UserServiceImpl() {
    }

    @Value("${helloMessageRegUser.user}")
    private String helloMessageRegUser;

    @Value("${helloMessageRegistration.user}")
    private String helloMessageRegistration;

    @Value("${helloMessageCodeActivation.user}")
    private String helloMessageCodeActivation;

    @Value("${offsetTime.user}")
    private long offsetTime;

    @Autowired
    @Qualifier("SendMessageToUse")
    public void setUserSendMessage(SendMessage userSendMessage){
        this.userSendMessage = userSendMessage;
    }

    @Autowired
    public void setUserRepository(UserRepository userRepository){
        this.userRepository = userRepository;
    }

    private String sendActivationCode(UserInfo userInfo) {
        String number = "";
        for (int i = 0; i < 4; i++) {
            int num = (int) (Math.random() * 10);
            number += String.valueOf(num);
        }
        String message = "Код активации акаунта " + number;
        userSendMessage.setDestination(userInfo.getEmail());
        userSendMessage.sendMessage(message);
        return number;
    }

    private String sendPassword(UserInfo userInfo) {
        String message = "Ваш пароль для входа " + userInfo.getPassword();
        userSendMessage.setDestination(userInfo.getEmail());
        userSendMessage.sendMessage(message);
        return null;
    }

    private String getHelloMessageRegUser(UserInfo userInfo){
        return helloMessageRegUser.replace("userName", userInfo.getFirstName());
    }

    private String getHelloMessageRegistration(){
        return helloMessageRegistration;
    }

    private String getHelloMessageCodeActivation(){
        return helloMessageCodeActivation;
    }

    private UserInfo getUserInfoToView(UserInfo userInfo){
        UserInfo tempInfo = new UserInfo();
        tempInfo.setFirstName(userInfo.getFirstName());
        tempInfo.setLastName(userInfo.getLastName());
        tempInfo.setEmail(userInfo.getEmail());
        tempInfo.setPhone(userInfo.getPhone());
        tempInfo.setUuid(userInfo.getUuid());
        tempInfo.setPassword(userInfo.getPassword());
        return tempInfo;
    }

    private InfoToPage getInfoToPageToView(InfoToPage infoToPage){
        InfoToPage tempInfo = new InfoToPage();
        tempInfo.setUrnPage(infoToPage.getUrnPage());
        tempInfo.setMessage(infoToPage.getMessage());
        tempInfo.setCodeActivation(infoToPage.getCodeActivation());
        tempInfo.setCodeActivationFromUser(infoToPage.getCodeActivationFromUser());
        tempInfo.setUserInfo(infoToPage.getUserInfo());
        tempInfo.setMealsMenu(infoToPage.getMealsMenu());
        tempInfo.setUserMealsOrder(infoToPage.getUserMealsOrder());
        tempInfo.setTables(infoToPage.getTables());
        tempInfo.setAllTime(infoToPage.getAllTime());
        tempInfo.setBooking(infoToPage.getBooking());
        tempInfo.setSelectTable(infoToPage.getSelectTable());
        tempInfo.setSelectOrder(infoToPage.getSelectOrder());
        tempInfo.setUserMealsOrderReady((infoToPage.getUserMealsOrderReady()));
        tempInfo.setUserMealsOrderCompleted(infoToPage.getUserMealsOrderCompleted());
        return tempInfo;
    }

    @Override
    public InfoToPage getRegistrationForm() {
        return new InfoToPage(ListUserUI.registration, new UserInfo(),
                getHelloMessageRegistration(), null);
    }

    @Override
    public InfoToPage getRegistration(UserInfo userInfo) {
        UserInfo tempInfo = userRepository.getUserByEmail(userInfo.getEmail());
        if (tempInfo != null) {
            UserInfo userToView = new UserInfo();
            userToView.setFirstName(tempInfo.getFirstName());
            userToView.setEmail(tempInfo.getEmail());
            return new InfoToPage(ListUserUI.index,userToView,
                    getHelloMessageRegUser(tempInfo), null);
        };
        String codeActivation = sendActivationCode(userInfo);
        return new InfoToPage(ListUserUI.activationCodePage,
                userInfo, getHelloMessageCodeActivation(), codeActivation);
    }

    @Override
    public InfoToPage getEnterWithCodeActivation(UserInfo userInfo, String codeActivation,
                                                 String codeActivationFromUser) {
        if (!codeActivation.equals(codeActivationFromUser)) {
            return new InfoToPage(ListUserUI.activationCodePage, userInfo,
                    "Код не верный", codeActivation);
        }
        UserInfo userToView = userRepository.addUser(userInfo);
        return getStartOrderInfoToPage(ListUserUI.reservationAndOrder, userToView, getHelloMessageRegUser(userInfo));
    }

    @Override
    public InfoToPage getNewActivationCode(UserInfo userInfo) {
        String codeActivation = sendActivationCode(userInfo);
        return new InfoToPage(ListUserUI.activationCodePage,
                userInfo, getHelloMessageCodeActivation(), codeActivation);
    }

    @Override
    public InfoToPage getChangeEmail(UserInfo userInfo) {
        return new InfoToPage(ListUserUI.registration, userInfo, getHelloMessageRegistration(), null);
    }

    private InfoToPage checkPassword(UserInfo userInfo, UserInfo tempInfo){
        if (tempInfo.getPassword().equals(userInfo.getPassword())) {
            return getStartOrderInfoToPage(ListUserUI.reservationAndOrder, tempInfo, getHelloMessageRegUser(tempInfo));
        } else {
            return new InfoToPage(ListUserUI.wrongPassword, userInfo,
                    "Пароль не верный", null);
        }
    }

    @Override
    public InfoToPage getEntry(UserInfo userInfo) {
        UserInfo tempInfo = userRepository.getUserByEmail(userInfo.getEmail());
        if (tempInfo != null) {
            return checkPassword(userInfo, tempInfo);
        } else {
            tempInfo = userRepository.getUserByPhone(userInfo.getPhone());
            if (tempInfo != null) {
                return checkPassword(userInfo, tempInfo);
            } else {
                return new InfoToPage(ListUserUI.userNotFound, userInfo,
                        "Пользователь не найден", null);
            }
        }
    }

    private InfoToPage sendPasswordPage(UserInfo userInfo){
        sendPassword(userInfo);
        UserInfo userToView = getUserInfoToView(userInfo);
        userToView.setPassword(null);
        return new InfoToPage(ListUserUI.passwordSendToEmail, userToView,
                "Пароль отправлен на email", null);
    }

    @Override
    public InfoToPage getPassword(UserInfo userInfo) {
        UserInfo tempInfo = userRepository.getUserByEmail(userInfo.getEmail());
        String message;
        if (tempInfo != null) {
            return sendPasswordPage(tempInfo);
        } else {
            tempInfo = userRepository.getUserByPhone(userInfo.getPhone());
            if (tempInfo != null) {
                return sendPasswordPage(tempInfo);
            } else {
                message = "Ooops ... user not found ... from getPassword()";
                return new InfoToPage(ListUserUI.userErrorPage, userInfo,
                        message, null);
            }
        }
    }

    @Override
    public InfoToPage getEnterAfterGetPassword(UserInfo userInfo) {
        UserInfo userToView = getUserInfoToView(userInfo);
        return new InfoToPage(ListUserUI.index, userToView, null, null);
    }

    private InfoToPage getStartOrderInfoToPage(ListUserUI urnPage, UserInfo userInfo, String message){

        List<Meals> mealsFromRepo = userRepository.getMeals();
        List<Meals> soupMeals = new ArrayList<>();
        List<Meals> mainMeals = new ArrayList<>();
        List<Meals> drinkMeals = new ArrayList<>();
        for (Meals meals: mealsFromRepo) {
            if (meals.getType().equals("soup")) soupMeals.add(meals);
            if (meals.getType().equals("main")) mainMeals.add(meals);
            if (meals.getType().equals("drink")) drinkMeals.add(meals);
        }
        MealsMenu mealsMenu = new MealsMenu();
        mealsMenu.setSoupMeals(soupMeals);
        mealsMenu.setMainMeals(mainMeals);
        mealsMenu.setDrinkMeals(drinkMeals);
        mealsMenu.setUserChoice(null);

        UserMealsOrder userMealsOrder = new UserMealsOrder();
        userMealsOrder.setOrderMeals(new ArrayList<>());
        userMealsOrder.setTotalPrice(0.0);


        InfoToPage infoToPage = new InfoToPage();
        infoToPage.setMealsMenu(mealsMenu);
        infoToPage.setUserMealsOrder(userMealsOrder);
        infoToPage.setUrnPage(urnPage);
        infoToPage.setUserInfo(userInfo);
        infoToPage.setMessage(message);
        return infoToPage;
    }

    /**
     * Методы обслуживания после успешной регистрации или входа
     */
    @Override
    public InfoToPage geMakeOrder(InfoToPage infoFromPage){
        UserInfo userInfo = new UserInfo();
        userInfo.setFirstName("WAITER");
        userInfo.setLastName("EMPLOYEES");
        userInfo.setUuid(activeEmployees.getEmployeesUUID());
        return getStartOrderInfoToPage(ListUserUI.menuAndOrder, userInfo, "Выберите блюда из меню");
    }

    @Override
    public InfoToPage getMenuOrder(InfoToPage infoFromPage) {
        InfoToPage infoToPage = getInfoToPageToView(infoFromPage);
        infoToPage.getUserMealsOrder().setTableNumber(infoToPage.getSelectTable());
        infoToPage.setMessage("Выберите блюда из меню");
        infoToPage.setUrnPage(ListUserUI.menuAndOrder);
        if (infoToPage.getMealsMenu().getUserChoice() != null) {
            Meals userChoice = infoToPage.getMealsMenu().getUserChoice();
            userChoice.setStatus("Заказан");
            double totalPrice = infoToPage.getUserMealsOrder().getTotalPrice() +
                    Double.parseDouble(infoFromPage.getMealsMenu().getUserChoice().getPrice());
            infoToPage.getUserMealsOrder().setTotalPrice(totalPrice);
            List<Meals> userMeals = infoToPage.getUserMealsOrder().getOrderMeals();
            userMeals.add(userChoice);
            infoToPage.getUserMealsOrder().setOrderMeals(userMeals);
        }
        return infoToPage;
    }

    @Override
    public InfoToPage getMenuDelItem(InfoToPage infoFromPage) {
        InfoToPage infoToPage = getInfoToPageToView(infoFromPage);
        infoToPage.setMessage("Выберите блюда из меню");
        infoToPage.setUrnPage(ListUserUI.menuAndOrder);
        Meals userChoice = infoToPage.getMealsMenu().getUserChoice();
        List<Meals> meals = infoToPage.getUserMealsOrder().getOrderMeals();
        int index = -1;
        for (int i = 0; i < meals.size(); i++) {
            if (meals.get(i).getTitle().equals(userChoice.getTitle())) {
                index = i;
                break;
            }
        }
        if (index != -1) meals.remove(index);
        double totalPrice = infoToPage.getUserMealsOrder().getTotalPrice() -
                Double.parseDouble(infoFromPage.getMealsMenu().getUserChoice().getPrice());
        infoToPage.getUserMealsOrder().setTotalPrice(totalPrice);
        infoToPage.getUserMealsOrder().setOrderMeals(meals);
        return infoToPage;
    }

    @Override
    public InfoToPage getOrderEscape(InfoToPage infoFromPage) {
        InfoToPage infoToPage = getInfoToPageToView(infoFromPage);
        infoToPage.setMessage("Выберите блюда из меню");
        infoToPage.setUrnPage(ListUserUI.menuAndOrder);
        UserMealsOrder userMealsOrder = new UserMealsOrder();
        userMealsOrder.setOrderMeals(new ArrayList<>());
        userMealsOrder.setTotalPrice(0.0);
        infoToPage.setUserMealsOrder(userMealsOrder);
        return infoToPage;
    }

    @Override
    public InfoToPage getOrderStart(InfoToPage infoFromPage) {
        InfoToPage infoToPage = getInfoToPageToView(infoFromPage);
        UserMealsOrder userMealsOrder = infoToPage.getUserMealsOrder();
        userMealsOrder.setUserName(infoToPage.getUserInfo().getFirstName());
        userMealsOrder.setUserUUID(infoFromPage.getUserInfo().getUuid());
        userMealsOrder.setState("Заказан");
        userMealsOrder.setOrderTime(TimeService.getSecond(offsetTime));
        userMealsOrder.setOrderNumber(userRepository.addUserMealsOrder(userMealsOrder));
        infoToPage.setMessage("Заказ в работе ");
        infoToPage.setUrnPage(ListUserUI.orderMealsFinal);
        infoToPage.setUserMealsOrder(userMealsOrder);
        return infoToPage;
    }

    @Override
    public InfoToPage getOrderOut(InfoToPage infoFromPage) {
        String message = activeEmployees.getFirstName() + ", Login : " + activeEmployees.getLogin();
        InfoToPage infoToPage = getStartOrderInfoToPage(ListUserUI.index_waiter,
                infoFromPage.getUserInfo(), message );
        return infoToPage;
    }

    @Override
    public InfoToPage getUpdateListOrder(InfoToPage infoFromPage) {
        InfoToPage infoToPage = getInfoToPageToView(infoFromPage);
        infoToPage.setUrnPage(ListUserUI.index_waiter);
        List<UserMealsOrder> userMealsOrderReady = userRepository.getUserMealsOrderByState("Готов");
        infoToPage.setUserMealsOrderReady(userMealsOrderReady);
        List<UserMealsOrder> userMealsOrderCompleted = userRepository.getUserMealsOrderByState("Выполнен");
        if (userMealsOrderCompleted.size() > 3) {
            UserMealsOrder tmpOrder = userMealsOrderCompleted.get(0);
            tmpOrder.setState("Завершен");
            userRepository.saveUserMealsOrder(tmpOrder);
            userMealsOrderCompleted.remove(0);
        }
        infoToPage.setUserMealsOrderCompleted(userMealsOrderCompleted);
        return infoToPage;
    }

    @Override
    public InfoToPage getSetCompleted(InfoToPage infoFromPage) {
        InfoToPage infoToPage = getInfoToPageToView(infoFromPage);
        infoToPage.setUrnPage(ListUserUI.index_waiter);
        Integer selectOrder = infoToPage.getSelectOrder();
        List<UserMealsOrder> listOrdersReady = infoToPage.getUserMealsOrderReady();
        UserMealsOrder tmpOrder = new UserMealsOrder();
        boolean flag = false;
        for (UserMealsOrder order: listOrdersReady) {
            if (order.getOrderNumber().equals(selectOrder)){
                order.setState("Выполнен");
                userRepository.saveUserMealsOrder(order);
                flag = true;
                tmpOrder = order;
            }
        }
        if (flag) listOrdersReady.remove(tmpOrder);
        infoToPage.setUserMealsOrderReady(listOrdersReady);
        List<UserMealsOrder> userMealsOrderCompleted = userRepository.getUserMealsOrderByState("Выполнен");
        infoToPage.setUserMealsOrderCompleted(userMealsOrderCompleted);
        return infoToPage;
    }

    @Override
    public InfoToPage getOrderWaiter(InfoToPage infoFromPage) {
        UserInfo userInfo = new UserInfo();
        userInfo.setFirstName("WAITER");
        userInfo.setLastName("EMPLOYEES");
        userInfo.setUuid(activeEmployees.getEmployeesUUID());
        InfoToPage infoToPage = getStartOrderInfoToPage(ListUserUI.select_table, userInfo, "Выберите блюда из меню");
        infoToPage.setTables(userRepository.getTables());
        return infoToPage;
    }

    @Override
    public InfoToPage getSendOrderToMail(InfoToPage infoFromPage) {
        userSendMessage.setDestination(infoFromPage.getUserInfo().getEmail());
        userSendMessage.sendMessage("Ваш заказ № " + infoFromPage.getUserMealsOrder().getOrderNumber());
        String message = "Продолжим " + infoFromPage.getUserInfo().getFirstName() + "!";
        InfoToPage infoToPage = getStartOrderInfoToPage(ListUserUI.reservationAndOrder,
                infoFromPage.getUserInfo(), message);
        return infoToPage;
    }

    /**
     * From adminApp
     */
    private final Set<String> allTime = new HashSet<>(Arrays.asList("12:00", "13:00", "14:00", "15:00", "16:00", "17:00", "18:00", "19:00"));

    public List<DietTable> getFreeTables() {
        List<DietTable> tables = new ArrayList<>();
        for (DietTable dietTable : userRepository.getTables()){
            if(!dietTable.isStatus()){
                tables.add(dietTable);
            }
        }
        return tables;
    }

    public Set<String> getAllTime() {
        return allTime;
    }

    @Override
    public InfoToPage getTableOrder(InfoToPage infoFromPage){
        InfoToPage infoToPage = getInfoToPageToView(infoFromPage);
        infoToPage.setUrnPage(ListUserUI.bookingTables);
        infoToPage.setMessage("Выберите стол");
        infoToPage.setTables(getFreeTables());
        infoToPage.setAllTime(getAllTime());
        Booking booking = new Booking();
        booking.setNamePerson(infoToPage.getUserInfo().getFirstName());
        booking.setBookingPerson("Клиент");
        infoToPage.setBooking(booking);
        return infoToPage;
    }

    @Override
    public InfoToPage resultBookingTable(InfoToPage infoFromPage){
        InfoToPage infoToPage = getInfoToPageToView(infoFromPage);
        infoToPage.setUrnPage(ListUserUI.bookingResult);
        infoToPage.getBooking().setBookingStatus("Ожидает подтверждения");
        userRepository.addToBookingList(infoToPage.getBooking());

        List<DietTable> tableList = userRepository.getTables();
        for (DietTable element : tableList) {
            if (element.getNumberTable() == infoToPage.getBooking().getNumberOfTable()){
                element.setStatus(true);
                element.setUserName(infoToPage.getUserInfo().getFirstName());
                element.setUser(infoToPage.getUserInfo().getUuid());
                element.setBookingTime(TimeService.getSecond(offsetTime));
                userRepository.saveTable(element);
            }
        }

        return infoToPage;
    }

    @Override
    public Employees getEmployees(){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String login = authentication.getName();
        Employees employees = userRepository.getEmployeesLogin(login);
        activeEmployees = employees;
        return employees;
    }
}
