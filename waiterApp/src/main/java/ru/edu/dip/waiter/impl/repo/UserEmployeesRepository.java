package ru.edu.dip.waiter.impl.repo;

import org.springframework.data.repository.CrudRepository;
import ru.edu.dip.waiter.entity.Employees;

import java.util.ArrayList;

public interface UserEmployeesRepository extends CrudRepository<Employees, Long> {
    ArrayList<Employees> findByLogin(String login);
    ArrayList<Employees> findByRole(String role);
}
