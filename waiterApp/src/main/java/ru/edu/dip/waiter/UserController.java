package ru.edu.dip.waiter;

import org.springframework.web.servlet.mvc.support.RedirectAttributes;

public interface UserController {

    String getRegistrationForm (RedirectAttributes redirectAttrs);
    String getRegistration (InfoToPage infoFromPage,
                                   RedirectAttributes redirectAttrs);
    String getEnterWithCodeActivation (InfoToPage infoFromPage,
                                              RedirectAttributes redirectAttrs);
    String getNewActivationCode(InfoToPage infoFromPage,
                                           RedirectAttributes redirectAttrs);
    String getChangeEmail(InfoToPage infoFromPage,
                                     RedirectAttributes redirectAttrs);
    String getEntry(InfoToPage infoFromPage, RedirectAttributes redirectAttrs);
    String getPassword (InfoToPage infoFromPage, RedirectAttributes redirectAttrs);
    String getEnterAfterGetPassword (InfoToPage infoFromPage,
                                            RedirectAttributes redirectAttrs);
    String getMenuDelItem(InfoToPage infoFromPage,
                                 RedirectAttributes redirectAttrs);
    String getOrderEscape(InfoToPage infoFromPage,
                                 RedirectAttributes redirectAttrs);
    String getOrderStart(InfoToPage infoFromPage,
                                RedirectAttributes redirectAttrs);
    String getOrderOut(InfoToPage infoFromPage,
                              RedirectAttributes redirectAttrs);
    String getSendOrderToMail(InfoToPage infoFromPage,
                              RedirectAttributes redirectAttrs);
    String resultBookingTable(InfoToPage infoFromPage,
                                     RedirectAttributes redirectAttrs);
}
