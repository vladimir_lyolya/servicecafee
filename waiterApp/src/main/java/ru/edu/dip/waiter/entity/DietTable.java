package ru.edu.dip.waiter.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.UUID;

@Entity
@Table(name  = "table_tb")
public class DietTable {
    @Id
    @Column(name="id", nullable = false)
    private Integer id;
    private int numberTable;
    private int place;
    private boolean status;
    private UUID userUUID;
    private long bookingTime;
    private String userName;

    public DietTable() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getNumberTable() {
        return numberTable;
    }

    public void setNumberTable(int numberTable) {
        this.numberTable = numberTable;
    }

    public int getPlace() {
        return place;
    }

    public void setPlace(int place) {
        this.place = place;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public UUID getUser() {
        return userUUID;
    }

    public void setUser(UUID user) {
        this.userUUID = user;
    }

    public long getBookingTime() {
        return bookingTime;
    }

    public void setBookingTime(long bookingTime) {
        this.bookingTime = bookingTime;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @Override
    public String toString() {
        return "Table{" +
                "id=" + id +
                ", numberTable=" + numberTable +
                ", place=" + place +
                ", status=" + status +
                ", user=" + userUUID +
                ", bookingTime=" + bookingTime +
                ", userName='" + userName + '\'' +
                '}';
    }
}
