package ru.edu.dip.waiter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

/**
 *
 */
@SpringBootApplication
@EnableAsync
public class WaiterApp
{
    public static void main( String[] args )
    {
        System.out.println( "Start userApp" );
        SpringApplication.run(WaiterApp.class, args);
    }
}
