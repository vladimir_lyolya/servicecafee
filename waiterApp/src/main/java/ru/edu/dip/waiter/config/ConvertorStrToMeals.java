package ru.edu.dip.waiter.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.format.FormatterRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import ru.edu.dip.waiter.convertor.MealsToString;
import ru.edu.dip.waiter.convertor.StringToMeals;

@Configuration
public class ConvertorStrToMeals implements WebMvcConfigurer {

        @Override
        public void addFormatters(FormatterRegistry registry) {
            registry.addConverter(new StringToMeals());
            registry.addConverter(new MealsToString());
        }
}
