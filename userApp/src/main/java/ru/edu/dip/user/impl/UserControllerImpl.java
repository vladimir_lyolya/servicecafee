package ru.edu.dip.user.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import ru.edu.dip.user.*;

import java.util.ArrayList;

@Controller
@SessionAttributes(value = "infoToPage", types=InfoToPage.class)
public class UserControllerImpl implements UserController {

    private UserService userService;

    @Autowired
    public UserControllerImpl(UserService userService) {
        this.userService = userService;
    }

    @GetMapping(value="/login")
    public String getLoginURN(@ModelAttribute("infoToPage") InfoToPage infoToPage) {
        return infoToPage.getUrnPage().toString();
    }

    @GetMapping(value="/registration")
    public String getRegistrationURN(@ModelAttribute("infoToPage") InfoToPage infoToPage){
        return infoToPage.getUrnPage().toString();
    }

    @GetMapping(value="/booking")
    public String getBookingURN(@ModelAttribute("infoToPage") InfoToPage infoToPage){
        return infoToPage.getUrnPage().toString();
    }

    @GetMapping(value="/error")
    public String getErrorURN(@ModelAttribute("infoToPage") InfoToPage infoToPage){
        return infoToPage.getUrnPage().toString();
    }

    @Override
    @PostMapping(value="/getRegistrationForm")
    public String getRegistrationForm(RedirectAttributes redirectAttrs) {
        InfoToPage infoToPage = userService.getRegistrationForm();
        redirectAttrs.addFlashAttribute("infoToPage", infoToPage);
        return ListUserUI.getURN(infoToPage);
    }

    @Override
    @PostMapping(value="/getRegistration")
    public String getRegistration(@ModelAttribute("infoToPage") InfoToPage infoFromPage,
                                  RedirectAttributes redirectAttrs) {
        InfoToPage infoToPage = userService.getRegistration(infoFromPage.getUserInfo());
        redirectAttrs.addFlashAttribute("infoToPage", infoToPage);
        return ListUserUI.getURN(infoToPage);
    }

    @Override
    @PostMapping(value="/getEnterWithCodeActivation")
    public String getEnterWithCodeActivation(@ModelAttribute("infoToPage") InfoToPage infoFromPage,
                                                 RedirectAttributes redirectAttrs) {
        InfoToPage infoToPage = userService.getEnterWithCodeActivation(infoFromPage.getUserInfo(),
                infoFromPage.getCodeActivation(), infoFromPage.getCodeActivationFromUser());
        redirectAttrs.addFlashAttribute("infoToPage", infoToPage);
        return ListUserUI.getURN(infoToPage);
    }

    @Override
    @PostMapping(value="/getNewActivationCode")
    public String getNewActivationCode(@ModelAttribute("infoToPage") InfoToPage infoFromPage,
                                           RedirectAttributes redirectAttrs) {
        InfoToPage infoToPage = userService.getNewActivationCode(infoFromPage.getUserInfo());
        redirectAttrs.addFlashAttribute("infoToPage", infoToPage);
        return ListUserUI.getURN(infoToPage);
    }

    @Override
    @PostMapping(value="/getChangeEmail")
    public String getChangeEmail(@ModelAttribute("infoToPage") InfoToPage infoFromPage,
                                     RedirectAttributes redirectAttrs) {
        InfoToPage infoToPage = userService.getChangeEmail(infoFromPage.getUserInfo());
        redirectAttrs.addFlashAttribute("infoToPage", infoToPage);
        return ListUserUI.getURN(infoToPage);
    }

    @Override
    @PostMapping(value="/getEntry")
    public String getEntry(@ModelAttribute("infoToPage") InfoToPage infoFromPage,
                                 RedirectAttributes redirectAttrs) {
        InfoToPage infoToPage = userService.getEntry(infoFromPage.getUserInfo());
        redirectAttrs.addFlashAttribute("infoToPage", infoToPage);
        return ListUserUI.getURN(infoToPage);
    }

    @Override
    @PostMapping(value="/getPassword")
    public String getPassword(@ModelAttribute("infoToPage") InfoToPage infoFromPage,
                              RedirectAttributes redirectAttrs) {
        InfoToPage infoToPage = userService.getPassword(infoFromPage.getUserInfo());
        redirectAttrs.addFlashAttribute("infoToPage", infoToPage);
        return ListUserUI.getURN(infoToPage);
    }

    @Override
    @PostMapping(value="/getEnterAfterGetPassword")
    public String getEnterAfterGetPassword(@ModelAttribute("infoToPage") InfoToPage infoFromPage,
                                               RedirectAttributes redirectAttrs) {
        InfoToPage infoToPage = userService.getEnterAfterGetPassword(infoFromPage.getUserInfo());
        redirectAttrs.addFlashAttribute("infoToPage", infoToPage);
        return ListUserUI.getURN(infoToPage);
    }

    @PostMapping(value="/getMenuOrder")
    public String getMenuOrder(@ModelAttribute("infoToPage") InfoToPage infoFromPage,
                                           RedirectAttributes redirectAttrs) {
        InfoToPage infoToPage = userService.getMenuOrder(infoFromPage);
        redirectAttrs.addFlashAttribute("infoToPage", infoToPage);
        return ListUserUI.getURN(infoToPage);
    }

    @PostMapping(value="/getMenuDelItem")
    public String getMenuDelItem(@ModelAttribute("infoToPage") InfoToPage infoFromPage,
                               RedirectAttributes redirectAttrs) {
        InfoToPage infoToPage = userService.getMenuDelItem(infoFromPage);
        redirectAttrs.addFlashAttribute("infoToPage", infoToPage);
        return ListUserUI.getURN(infoToPage);
    }

    @PostMapping(value="/getOrderEscape")
    public String getOrderEscape(@ModelAttribute("infoToPage") InfoToPage infoFromPage,
                                 RedirectAttributes redirectAttrs) {
        InfoToPage infoToPage = userService.getOrderEscape(infoFromPage);
        redirectAttrs.addFlashAttribute("infoToPage", infoToPage);
        return ListUserUI.getURN(infoToPage);
    }


    @PostMapping(value="/getOrderOut")
    public String getOrderOut(@ModelAttribute("infoToPage") InfoToPage infoFromPage,
                                RedirectAttributes redirectAttrs) {
        InfoToPage infoToPage = userService.getOrderOut(infoFromPage);
        redirectAttrs.addFlashAttribute("infoToPage", infoToPage);
        return ListUserUI.getURN(infoToPage);
    }

    @PostMapping(value="/getOrderStart")
    public String getOrderStart(@ModelAttribute("infoToPage") InfoToPage infoFromPage,
                                 RedirectAttributes redirectAttrs) {
        InfoToPage infoToPage = userService.getOrderStart(infoFromPage);
        redirectAttrs.addFlashAttribute("infoToPage", infoToPage);
        return ListUserUI.getURN(infoToPage);
    }

    @PostMapping(value="/getSendOrderToMail")
    public String getSendOrderToMail(@ModelAttribute("infoToPage") InfoToPage infoFromPage,
                                RedirectAttributes redirectAttrs) {
        InfoToPage infoToPage = userService.getSendOrderToMail(infoFromPage);
        redirectAttrs.addFlashAttribute("infoToPage", infoToPage);
        return ListUserUI.getURN(infoToPage);
    }


    @PostMapping(value="/getTableOrder")
    public String getTableOrder(@ModelAttribute("infoToPage") InfoToPage infoFromPage,
                                     RedirectAttributes redirectAttrs) {
        InfoToPage infoToPage = userService.getTableOrder(infoFromPage);
        redirectAttrs.addFlashAttribute("infoToPage", infoToPage);
        return ListUserUI.getURN(infoToPage);
    }

    @PostMapping(value="/resultBookingTable")
    public String resultBookingTable(@ModelAttribute("infoToPage") InfoToPage infoFromPage,
                                RedirectAttributes redirectAttrs) {
        InfoToPage infoToPage = userService.resultBookingTable(infoFromPage);
        redirectAttrs.addFlashAttribute("infoToPage", infoToPage);
        return ListUserUI.getURN(infoToPage);
    }

    @GetMapping(value="/")
    public String index(RedirectAttributes redirectAttrs){
        InfoToPage infoToPage = new InfoToPage();
        infoToPage.setMessage("Добро пожаловать !");
        infoToPage.setUrnPage(ListUserUI.index);
        redirectAttrs.addFlashAttribute("infoToPage", infoToPage);
        return ListUserUI.getURN(infoToPage);
    }
}
