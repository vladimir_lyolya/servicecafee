package ru.edu.dip.user;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

/**
 *
 */
@SpringBootApplication
@EnableAsync
public class UserApp
{
    public static void main( String[] args )
    {
        System.out.println( "Start userApp" );
        SpringApplication.run(UserApp.class, args);
    }
}
