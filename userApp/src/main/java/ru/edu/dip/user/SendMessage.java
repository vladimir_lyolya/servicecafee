package ru.edu.dip.user;

public interface SendMessage {

    public String sendMessage(String message);
    public void setDestination(String destination);
}
