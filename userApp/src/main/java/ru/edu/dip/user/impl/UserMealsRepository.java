package ru.edu.dip.user.impl;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.edu.dip.user.entity.Meals;

@Repository
public interface UserMealsRepository extends CrudRepository<Meals,Long> {

}
