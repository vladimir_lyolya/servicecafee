package ru.edu.dip.user.impl;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.edu.dip.user.entity.UserMealsOrderDAO;

import java.util.ArrayList;
import java.util.UUID;

@Repository
public interface UserMealsOrderRepository extends CrudRepository<UserMealsOrderDAO, Long> {

    ArrayList<UserMealsOrderDAO> findByOrderNumber(Integer orderNumber);
    ArrayList<UserMealsOrderDAO> findByUserUUID(UUID userUUID);
    ArrayList<UserMealsOrderDAO> findByState(String state);
}
