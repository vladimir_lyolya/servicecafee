package ru.edu.dip.user.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import ru.edu.dip.user.UserRepository;
import ru.edu.dip.user.entity.Employees;

import java.util.ArrayList;

public class UserAppDetailedService implements UserDetailsService {

    private UserRepository userRepository;

    @Autowired
    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Employees employees = userRepository.getEmployeesLogin(username);
        if (!username.equals(employees.getLogin())) {
            throw new UsernameNotFoundException("Сотрудник " + username + " не найден!");
        }

        return new User(employees.getLogin(), employees.getPassword(), new ArrayList<>());
    }
}
