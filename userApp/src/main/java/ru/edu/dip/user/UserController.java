package ru.edu.dip.user;

import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

public interface UserController {

    public String getRegistrationForm (RedirectAttributes redirectAttrs);
    public String getRegistration (InfoToPage infoFromPage,
                                   RedirectAttributes redirectAttrs);
    public String getEnterWithCodeActivation (InfoToPage infoFromPage,
                                              RedirectAttributes redirectAttrs);
    public String getNewActivationCode(InfoToPage infoFromPage,
                                           RedirectAttributes redirectAttrs);
    public String getChangeEmail(InfoToPage infoFromPage,
                                     RedirectAttributes redirectAttrs);
    public String getEntry(InfoToPage infoFromPage, RedirectAttributes redirectAttrs);
    public String getPassword (InfoToPage infoFromPage, RedirectAttributes redirectAttrs);
    public String getEnterAfterGetPassword (InfoToPage infoFromPage,
                                            RedirectAttributes redirectAttrs);
    public String getMenuOrder(InfoToPage infoFromPage,
                               RedirectAttributes redirectAttrs);
    public String getMenuDelItem(InfoToPage infoFromPage,
                                 RedirectAttributes redirectAttrs);
    public String getOrderEscape(InfoToPage infoFromPage,
                                 RedirectAttributes redirectAttrs);
    public String getOrderStart(InfoToPage infoFromPage,
                                RedirectAttributes redirectAttrs);
    public String getOrderOut(InfoToPage infoFromPage,
                              RedirectAttributes redirectAttrs);
    public String getSendOrderToMail(InfoToPage infoFromPage,
                              RedirectAttributes redirectAttrs);
    public String resultBookingTable(InfoToPage infoFromPage,
                                     RedirectAttributes redirectAttrs);
}
