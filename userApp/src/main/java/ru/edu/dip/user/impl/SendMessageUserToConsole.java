package ru.edu.dip.user.impl;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import ru.edu.dip.user.SendMessage;

@Component

public class SendMessageUserToConsole implements SendMessage {

    private String destination;

    public SendMessageUserToConsole () {

    }

    @Override
    public String sendMessage(String message) {
        System.out.println("\n" + message + " отправлен на " + destination +"\n");
        return "ok";
    }

    @Override
    public void setDestination(String destination) {
        this.destination = destination;
    }

}
