package ru.edu.dip.user.impl;

import org.springframework.data.repository.CrudRepository;
import ru.edu.dip.user.entity.Employees;

import java.util.ArrayList;

public interface UserEmployeesRepository extends CrudRepository<Employees, Long> {
    ArrayList<Employees> findByLogin(String login);
    ArrayList<Employees> findByRole(String role);
}
