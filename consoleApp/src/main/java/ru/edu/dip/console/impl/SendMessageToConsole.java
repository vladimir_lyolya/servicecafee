package ru.edu.dip.console.impl;

import ru.edu.dip.console.SendMessage;

//@Component
public class SendMessageToConsole implements SendMessage {
    private String destination;
    @Override
    public String sendMessage(String message) {
        System.out.println("\nmessage " + message + " send to " + destination +"\n");
        return "ok";
    }

    @Override
    public String setDestination(String destination) {
        this.destination = destination;
        return null;
    }

}
