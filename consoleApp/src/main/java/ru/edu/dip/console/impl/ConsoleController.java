package ru.edu.dip.console.impl;

import ru.edu.dip.console.*;

//@Controller
public class ConsoleController implements UserController {

    private UserService userService;

    //@Autowired
    public ConsoleController(UserService userService) {
        this.userService = userService;
    }

    @Override
    public InfoToPage getRegistrationForm() {
        return userService.getRegistrationForm();
    }

    @Override
    public InfoToPage getRegistration(UserInfo userInfo) {
        return userService.getRegistration(userInfo);
    }

    @Override
    public InfoToPage getEnterWithCodeActivation(UserInfo userInfo, InfoFromUser infoFromUser) {
        return userService.getEnterWithCodeActivation(userInfo, infoFromUser);
    }

    @Override
    public InfoToPage getNewActivationCode(UserInfo userInfo) {
        return userService.getNewActivationCode(userInfo);
    }

    @Override
    public InfoToPage getChangeEmail(UserInfo userInfo) {
        return userService.getChangeEmail(userInfo);
    }

    @Override
    public InfoToPage getEntry(UserInfo userInfo) {
        return userService.getEntry(userInfo);
    }

    @Override
    public InfoToPage getPassword(UserInfo userInfo) {
        return userService.getPassword(userInfo);
    }

    @Override
    public InfoToPage getEnterAfterGetPassword(UserInfo userInfo) {
        return userService.getEnterAfterGetPassword(userInfo);
    }
}
