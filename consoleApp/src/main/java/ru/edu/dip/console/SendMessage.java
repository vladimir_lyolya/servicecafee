package ru.edu.dip.console;

public interface SendMessage {

    public String sendMessage(String message);
    public String setDestination(String Destination);
}
