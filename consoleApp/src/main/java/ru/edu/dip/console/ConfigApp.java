package ru.edu.dip.console;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.edu.dip.console.impl.ConsoleController;
import ru.edu.dip.console.impl.ConsoleRepository;
import ru.edu.dip.console.impl.ConsoleService;
import ru.edu.dip.console.impl.SendMessageToConsole;

@Configuration
public class ConfigApp {

    //@Bean
    //public ConsoleMenu consoleMenu(){
    //    return new ConsoleMenu(userController());
    //}

    @Bean
    public UserController userController(){
        return new ConsoleController(userService());
    }

    @Bean
    public SendMessage sendMessage(){
        return new SendMessageToConsole();
    }

    @Bean
    public UserService userService(){
        return new ConsoleService(userRepository(), sendMessage());
    }

    @Bean
    public UserRepository userRepository(){
        return new ConsoleRepository();
    }
}
