package ru.edu.dip.console;

public class InfoFromUser {

    private String codeActivationFromUser;
    private String codeActivation;

    public InfoFromUser(String codeActivation) {
        this.codeActivation = codeActivation;
    }

    public String getCodeActivation() {
        return codeActivation;
    }

    public void setCodeActivation(String codeActivation) {
        this.codeActivation = codeActivation;
    }

    public String getCodeActivationFromUser() {
        return codeActivationFromUser;
    }

    public void setCodeActivationFromUser(String codeActivationFromUser) {
        this.codeActivationFromUser = codeActivationFromUser;
    }

}
