package ru.edu.dip.console.impl;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.edu.dip.console.UserController;

public class App {
    public static void main(String[] args) {

        ApplicationContext applicationContext = new AnnotationConfigApplicationContext("ru.edu.dip.user");
        UserController userController = applicationContext.getBean(ConsoleController.class);
        ConsoleMenu consoleMenu = new ConsoleMenu(userController);
        consoleMenu.goMenu();
        ((AnnotationConfigApplicationContext)applicationContext).close();
    }
}
