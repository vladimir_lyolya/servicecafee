package ru.edu.dip.console;

public interface UserController {

    public InfoToPage getRegistrationForm ();
    public InfoToPage getRegistration (UserInfo userInfo);
    public InfoToPage getEnterWithCodeActivation (UserInfo userInfo, InfoFromUser infoFromUser);
    public InfoToPage getNewActivationCode(UserInfo userInfo);
    public InfoToPage getChangeEmail(UserInfo userInfo);
    public InfoToPage getEntry(UserInfo userInfo);
    public InfoToPage getPassword (UserInfo userInfo);
    public InfoToPage getEnterAfterGetPassword (UserInfo userInfo);

}
