package ru.edu.dip.console.impl;

import org.springframework.stereotype.Repository;
import ru.edu.dip.console.UserInfo;
import ru.edu.dip.console.UserRepository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Repository
public class ConsoleRepository implements UserRepository {

    private Map<UUID,UserInfo> repo = new HashMap<>();

    @Override
    public UserInfo getUserByPhone(String phone) {
        ArrayList<UserInfo> userInfos = new ArrayList(repo.values());
        UserInfo tempInfo = null;
        for (UserInfo userInfo:userInfos) {
            if (phone.equals(userInfo.getPhone())){
                tempInfo = userInfo;
                break;
            }
        }
        return tempInfo;
    }

    @Override
    public UserInfo getUserByEmail(String email) {
        ArrayList<UserInfo> userInfos = new ArrayList(repo.values());
        UserInfo tempInfo = null;
        for (UserInfo userInfo:userInfos) {
            if (email.equals(userInfo.getEmail())){
                tempInfo = userInfo;
                break;
            }
        }
        return tempInfo;
    }

    @Override
    public void addUser(UserInfo userInfo) {
        UUID uuid = UUID.randomUUID();
        repo.put(uuid, userInfo);
    }
}
