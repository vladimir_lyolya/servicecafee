package ru.edu.dip.console.impl;

import ru.edu.dip.console.UserController;
import ru.edu.dip.console.InfoToPage;
import ru.edu.dip.console.ListUserUI;
import ru.edu.dip.console.UserInfo;

import java.util.Scanner;

public class ConsoleMenu {

    boolean flagExit;
    Scanner input;
    UserController userController;

    public ConsoleMenu(UserController userController) {
        this.userController = userController;
    }

    public void goMenu(){
        flagExit = false;
        input = new Scanner(System.in);
        while(!flagExit) {
            System.out.println("Меню:");
            System.out.println("1 - Войти");
            System.out.println("2 - Регистрация");
            System.out.println("3 - Выход");
            switch (input.nextInt()) {
                case 1:
                    goEntry();
                    break;
                case 2:
                    goRegistration();
                    break;
                case 3:
                    flagExit = true;
                    break;
                default: {
                    System.out.println("Используйте цифры 1 - 3");
                }
            }
        }
    }
    private boolean checkReturn(String msg){
        try {
            if (Integer.valueOf(msg) == 3) return true;
        } catch (Exception ex){
            return false;
        }
        return false;
    }

    public void goEntry(){
        System.out.println("Page loginWithEmail //нажмите 3 - для возврата//");
        System.out.println("Введите email или телефон:");
        String msg1 = input.next();
        if (checkReturn(msg1)) return;
        System.out.println("Введите пароль");
        String msg2 = input.next();
        if (checkReturn(msg2)) return;
        InfoToPage info1 = userController.getEntry(new UserInfo(null,null,msg1,msg1, null, msg2));
        if (info1.getUrnPage().equals(ListUserUI.userNotFound)) {
            System.out.println(info1.getErrorMessage());
            System.out.println("Для перехода к регистрации нажмите 4 или 0 для продолжения");
            msg1 = input.next();
            if (Integer.valueOf(msg1) == 4) goRegistration();
            System.out.println("Введите email или телефон:");
            msg1 = input.next();
            if (checkReturn(msg1)) return;
            System.out.println("Введите пароль");
            msg2 = input.next();
            if (checkReturn(msg2)) return;
            info1 = userController.getEntry(new UserInfo(null,null,msg1,msg1, null, msg2));
            System.out.println(info1.getErrorMessage());
            return;
        } else {
            if (info1.getUrnPage().equals(ListUserUI.wrongPassword)) {

            } else {
                System.out.println("Error page not found");
                flagExit = true;
                return;
            }
        }

        flagExit = true;
    }

    public void goRegistration(){
        System.out.println("Page registration //нажмите 3 - для возврата//");
        String msg1 = input.next();
        if (checkReturn(msg1)) System.out.println("Возврат");;
        flagExit = true;
    }
}
