package ru.edu.dip.console;

public class InfoToPage {

    private ListUserUI urnPage;
    private String errorMessage;

    private String codeActivation;
    private UserInfo userInfo;

    public InfoToPage(){

    }

    public InfoToPage(ListUserUI urnPage, UserInfo userInfo,
                      String errorMessage, String codeActivation) {
        this.urnPage = urnPage;
        this.userInfo = userInfo;
        this.errorMessage = errorMessage;
        this.codeActivation = codeActivation;
    }

    public ListUserUI getUrnPage() {
        return urnPage;
    }

    public void setUrnPage(ListUserUI urnPage) {
        this.urnPage = urnPage;
    }

    public UserInfo getUserInfo() {
        return userInfo;
    }

    public void setUserInfo(UserInfo userInfo) {
        this.userInfo = userInfo;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getCodeActivation() {
        return codeActivation;
    }

    public void setCodeActivation(String codeActivation) {
        this.codeActivation = codeActivation;
    }
}
