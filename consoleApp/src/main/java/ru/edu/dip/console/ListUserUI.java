package ru.edu.dip.console;

public enum ListUserUI {
    errorPage,
    loginWithEmail,
    activationCodePage,
    activationCodeWrong,
    registration,
    registrationWithInfo,
    userNotFound,
    wrongPassword,
    passwordSendToEmail,
    reservationAndOrder
}
