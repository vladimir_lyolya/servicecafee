package ru.edu.dip.console.impl;

import org.springframework.stereotype.Service;
import ru.edu.dip.console.*;

@Service
public class ConsoleService implements UserService {

    private UserRepository userRepository;
    private SendMessage userSendMessage;

    //@Autowired
    public ConsoleService(UserRepository userRepository, SendMessage userSendMessage) {
        this.userRepository = userRepository;
        this.userSendMessage = userSendMessage;
    }

    private String sendActivationCode() {
        String number = "";
        for (int i = 0; i < 4; i++) {
            int num = (int) (Math.random() * 10);
            number += String.valueOf(num);
        }
        String message = "Код активации акаунта " + number;
        userSendMessage.sendMessage(message);
        return number;
    }

    private String sendPassword(UserInfo userInfo) {
        String message = "Ваш пароль для входа " + userInfo.getPassword();
        userSendMessage.setDestination(userInfo.getEmail());
        userSendMessage.sendMessage(message);
        return null;
    }

    @Override
    public InfoToPage getRegistrationForm() {
        return new InfoToPage(ListUserUI.registration, null, null, null);
    }

    @Override
    public InfoToPage getRegistration(UserInfo userInfo) {
        UserInfo tempInfo = userRepository.getUserByEmail(userInfo.getEmail());
        if (tempInfo != null) {
            return new InfoToPage(ListUserUI.loginWithEmail,tempInfo,
                    null, null);
        };
        String codeActivation = sendActivationCode();
        return new InfoToPage(ListUserUI.activationCodePage,
                userInfo, null, codeActivation);
    }

    @Override
    public InfoToPage getEnterWithCodeActivation(UserInfo userInfo, InfoFromUser infoFromUser) {
        if (!infoFromUser.getCodeActivationFromUser().equals(infoFromUser.getCodeActivation())) {
            return new InfoToPage(ListUserUI.activationCodeWrong, userInfo,
                    "Код не верный", infoFromUser.getCodeActivation());
        }
        userRepository.addUser(userInfo);
        return new InfoToPage(ListUserUI.reservationAndOrder, userInfo, null, null);
    }

    @Override
    public InfoToPage getNewActivationCode(UserInfo userInfo) {
        String codeActivation = sendActivationCode();
        return new InfoToPage(ListUserUI.activationCodePage,
                userInfo, null, codeActivation);
    }

    @Override
    public InfoToPage getChangeEmail(UserInfo userInfo) {
        return new InfoToPage(ListUserUI.registrationWithInfo, userInfo, null, null);
    }

    private InfoToPage checkPassword(UserInfo userInfo, UserInfo tempInfo){
        if (tempInfo.getPassword().equals(userInfo.getPassword())) {
            return new InfoToPage(ListUserUI.reservationAndOrder, tempInfo,
                    null, null);
        } else {
            return new InfoToPage(ListUserUI.wrongPassword, userInfo,
                    "Пароль не верный", null);
        }
    }

    @Override
    public InfoToPage getEntry(UserInfo userInfo) {
        UserInfo tempInfo = userRepository.getUserByEmail(userInfo.getEmail());
        if (tempInfo != null) {
            return checkPassword(userInfo, tempInfo);
        } else {
            tempInfo = userRepository.getUserByPhone(userInfo.getPhone());
            if (tempInfo != null) {
                return checkPassword(userInfo, tempInfo);
            } else {
                return new InfoToPage(ListUserUI.userNotFound, userInfo,
                        "Пользователь не найден", null);
            }
        }
    }

    private InfoToPage sendPasswordPage(UserInfo userInfo){
        sendPassword(userInfo);
        userInfo.setPassword(null);
        return new InfoToPage(ListUserUI.passwordSendToEmail, userInfo,
                "Пароль отправлен на email", null);
    }

    @Override
    public InfoToPage getPassword(UserInfo userInfo) {
        UserInfo tempInfo = userRepository.getUserByEmail(userInfo.getEmail());
        String message;
        if (tempInfo != null) {
            return sendPasswordPage(tempInfo);
        } else {
            tempInfo = userRepository.getUserByPhone(userInfo.getPhone());
            if (tempInfo != null) {
                return sendPasswordPage(tempInfo);
            } else {
                message = "Ooops ... user not found ... from getPassword()";
                return new InfoToPage(ListUserUI.errorPage, userInfo,
                        message, null);
            }
        }
    }

    @Override
    public InfoToPage getEnterAfterGetPassword(UserInfo userInfo) {
        return new InfoToPage(ListUserUI.loginWithEmail, userInfo, null, null);
    }
}
