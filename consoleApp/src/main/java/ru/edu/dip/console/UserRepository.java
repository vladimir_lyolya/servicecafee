package ru.edu.dip.console;

public interface UserRepository {

    public UserInfo getUserByPhone(String phone);
    public UserInfo getUserByEmail(String email);
    public void addUser(UserInfo userInfo);

}
