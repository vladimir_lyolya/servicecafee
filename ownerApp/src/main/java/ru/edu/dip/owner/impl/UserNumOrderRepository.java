package ru.edu.dip.owner.impl;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.edu.dip.owner.entity.NumberOrder;

@Repository
public interface UserNumOrderRepository extends CrudRepository<NumberOrder, Integer> {

}
