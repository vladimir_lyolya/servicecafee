package ru.edu.dip.owner.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;
import java.util.UUID;

@Entity
@Table(name="user_meals_order")
public class UserMealsOrderDAO {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    private Integer orderNumber;
    private String state;
    private Integer tableNumber;
    private String userName;
    private UUID userUUID;
    private long orderTime;
    private String bookingTime;
    private double totalPrice;

    @OneToMany(cascade= CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "order_meals_id")
    private List<OrderMeals> orderMeals;

    public UserMealsOrderDAO() {
    }

    public UserMealsOrderDAO(Integer orderNumber, String state, Integer tableNumber, String userName, UUID userUUID, long orderTime, double totalPrice, List<OrderMeals> orderMeals) {
        this.orderNumber = orderNumber;
        this.state = state;
        this.tableNumber = tableNumber;
        this.userName = userName;
        this.userUUID = userUUID;
        this.orderTime = orderTime;
        this.totalPrice = totalPrice;
        this.orderMeals = orderMeals;
    }

    public List<OrderMeals> getOrderMeals() {
        return orderMeals;
    }

    public void setOrderMeals(List<OrderMeals> orderMeals) {
        this.orderMeals = orderMeals;
    }

    public Integer getTableNumber() {
        return tableNumber;
    }

    public void setTableNumber(Integer tableNumber) {
        this.tableNumber = tableNumber;
    }

    public UUID getUserUUID() {
        return userUUID;
    }

    public void setUserUUID(UUID userUUID) {
        this.userUUID = userUUID;
    }

    public long getOrderTime() {
        return orderTime;
    }

    public void setOrderTime(long orderTime) {
        this.orderTime = orderTime;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Integer getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(Integer orderNumber) {
        this.orderNumber = orderNumber;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBookingTime() {
        return bookingTime;
    }

    public void setBookingTime(String bookingTime) {
        this.bookingTime = bookingTime;
    }

    @Override
    public String toString() {
        return "UserMealsOrderDAO{" +
                "id=" + id +
                ", orderNumber=" + orderNumber +
                ", state='" + state + '\'' +
                ", tableNumber=" + tableNumber +
                ", userName='" + userName + '\'' +
                ", userUUID=" + userUUID +
                ", orderTime=" + orderTime +
                ", bookingTime='" + bookingTime + '\'' +
                ", totalPrice=" + totalPrice +
                ", orderMeals=" + orderMeals +
                '}';
    }
}

