package ru.edu.dip.owner.impl;

import ru.edu.dip.owner.entity.Booking;
import ru.edu.dip.owner.entity.DietTable;
import ru.edu.dip.owner.entity.Employees;
import ru.edu.dip.owner.entity.Meals;
import ru.edu.dip.owner.entity.UserInfo;
import ru.edu.dip.owner.entity.UserMealsOrder;

import java.util.List;
import java.util.UUID;

public interface UserRepository {

    UserInfo getUserByPhone(String phone);
    UserInfo getUserByEmail(String email);
    UserInfo addUser(UserInfo userInfo);

    List<Meals> getMeals();
    void setMeals(List<Meals> listMeals);

    Integer addUserMealsOrder(UserMealsOrder order);
    UserMealsOrder getUserMealsOrder(Integer key);
    List<UserMealsOrder> getUserMealsOrder(UUID userUUID);
    List<UserMealsOrder> getUserMealsOrderAll();
    List<UserMealsOrder> getUserMealsOrderByState(String state);
    public void saveUserMealsOrder(UserMealsOrder order);

    DietTable getTable(UUID userUuid);
    void saveTable(DietTable dietTable);
    List<DietTable> getTables();

    void addToBookingList(Booking booking);
    List<Booking> getBookingList();
    void removeBookingTable(int numberOfTable);

    void setEmployees(Employees employees);
    List<Employees> getEmployeesRole(String role);
    Employees getEmployeesLogin(String login);

}
