package ru.edu.dip.owner.entity;

import java.util.List;
import java.util.UUID;

public class UserMealsOrder {

    private Long id;
    private Integer orderNumber;
    private String state;
    private Integer tableNumber;
    private String userName;
    private UUID userUUID;
    private long orderTime;
    private String bookingTime;
    private double totalPrice;
    private List<Meals> orderMeals;

    public UserMealsOrder() {
    }

    public UserMealsOrder(Integer orderNumber, String state, Integer tableNumber, String userName, UUID userUUID, long orderTime, double totalPrice, List<Meals> orderMeals) {
        this.orderNumber = orderNumber;
        this.state = state;
        this.tableNumber = tableNumber;
        this.userName = userName;
        this.userUUID = userUUID;
        this.orderTime = orderTime;
        this.totalPrice = totalPrice;
        this.orderMeals = orderMeals;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<Meals> getOrderMeals() {
        return orderMeals;
    }

    public void setOrderMeals(List<Meals> orderMeals) {
        this.orderMeals = orderMeals;
    }

    public Integer getTableNumber() {
        return tableNumber;
    }

    public void setTableNumber(Integer tableNumber) {
        this.tableNumber = tableNumber;
    }

    public UUID getUserUUID() {
        return userUUID;
    }

    public void setUserUUID(UUID userUUID) {
        this.userUUID = userUUID;
    }

    public long getOrderTime() {
        return orderTime;
    }

    public void setOrderTime(long orderTime) {
        this.orderTime = orderTime;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Integer getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(Integer orderNumber) {
        this.orderNumber = orderNumber;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getBookingTime() {
        return bookingTime;
    }

    public void setBookingTime(String bookingTime) {
        this.bookingTime = bookingTime;
    }

    @Override
    public String toString() {
        return "UserMealsOrder{" +
                "id=" + id +
                ", orderNumber=" + orderNumber +
                ", state='" + state + '\'' +
                ", tableNumber=" + tableNumber +
                ", userName='" + userName + '\'' +
                ", userUUID=" + userUUID +
                ", orderTime=" + orderTime +
                ", bookingTime='" + bookingTime + '\'' +
                ", totalPrice=" + totalPrice +
                ", orderMeals=" + orderMeals +
                '}';
    }
}
