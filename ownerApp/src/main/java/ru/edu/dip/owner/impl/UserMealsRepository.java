package ru.edu.dip.owner.impl;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.edu.dip.owner.entity.Meals;

@Repository
public interface UserMealsRepository extends CrudRepository<Meals,Long> {

}
