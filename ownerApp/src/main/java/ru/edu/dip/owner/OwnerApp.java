package ru.edu.dip.owner;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 *
 *
 */
@SpringBootApplication
public class OwnerApp
{
    public static void main( String[] args )
    {
        System.out.println( "Start ownerApp" );
        SpringApplication.run(OwnerApp.class, args);
    }
}
