package ru.edu.dip.owner.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.edu.dip.owner.entity.Booking;
import ru.edu.dip.owner.entity.DietTable;
import ru.edu.dip.owner.entity.Employees;
import ru.edu.dip.owner.entity.Meals;
import ru.edu.dip.owner.entity.NumberOrder;
import ru.edu.dip.owner.entity.OrderMeals;
import ru.edu.dip.owner.entity.UserInfo;
import ru.edu.dip.owner.entity.UserMealsOrder;
import ru.edu.dip.owner.entity.UserMealsOrderDAO;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Component
public class UserRepositoryImpl implements UserRepository {

    private UserInfoRepository userInfoRepository;
    private UserMealsRepository userMealsRepository;
    private UserMealsOrderRepository userMealsOrderRepository;
    private UserNumOrderRepository userNumsOrderRepository;
    private UserBookingRepository userBookingRepository;
    private UserTableRepository userTableRepository;
    private UserEmployeesRepository userEmployeesRepository;

    NumberOrder numberOrder;
    private int idRepoOrder;

    @Autowired
    public void setUserEmployeesRepository(UserEmployeesRepository userEmployeesRepository) {
        this.userEmployeesRepository = userEmployeesRepository;
    }

    @Autowired
    public void setUserTableRepository(UserTableRepository userTableRepository) {
        this.userTableRepository = userTableRepository;
    }

    @Autowired
    public void setUserBookingRepository(UserBookingRepository userBookingRepository) {
        this.userBookingRepository = userBookingRepository;
    }

    @Autowired
    public void setUserInfoRepository(UserInfoRepository userInfoRepository) {
        this.userInfoRepository = userInfoRepository;
    }

    @Autowired
    public void setUserMealsRepository(UserMealsRepository userMealsRepository) {
        this.userMealsRepository = userMealsRepository;
    }

    @Autowired
    public void setUserMealsOrderRepository(UserMealsOrderRepository userMealsOrderRepository) {
        this.userMealsOrderRepository = userMealsOrderRepository;
    }

    @Autowired
    public void setUserNumOrderRepository(UserNumOrderRepository userNumsOrderRepository) {
        this.userNumsOrderRepository = userNumsOrderRepository;
    }

    @PostConstruct
    private void init(){
        numberOrder = new NumberOrder();
        numberOrder.setId(1);
        Optional<NumberOrder> numFromDB = userNumsOrderRepository.findById(1);
        if (numFromDB.isPresent()) {
            idRepoOrder = numFromDB.get().getNumberOrder();
        } else {
            idRepoOrder = 0;
        }
    }

    @Override
    public Employees getEmployeesLogin(String login){
        ArrayList<Employees> employeesList = userEmployeesRepository.findByLogin(login);
        if (employeesList.size() > 0) return employeesList.get(0);
        return new Employees();
    }

    @Override
    public List<Employees> getEmployeesRole(String role){
        ArrayList<Employees> employeesList = userEmployeesRepository.findByRole(role);
        return employeesList;
    }

    @Override
    public void setEmployees(Employees employees){
        userEmployeesRepository.save(employees);
    }

    @Override
    public UserInfo getUserByPhone(String phone) {
        if (phone == null) return null;
        ArrayList<UserInfo> userInfos = userInfoRepository.findByPhone(phone);
        UserInfo tempInfo = null;
        for (UserInfo userInfo:userInfos) {
            if (phone.equals(userInfo.getPhone())){
                tempInfo = userInfo;
                break;
            }
        }
        return tempInfo;
    }

    @Override
    public UserInfo getUserByEmail(String email) {
        if (email == null) return null;
        ArrayList<UserInfo> userInfos = userInfoRepository.findByEmail(email);
        UserInfo tempInfo = null;
        for (UserInfo user:userInfos) {
            if (email.equals(user.getEmail())){
                tempInfo = user;
                break;
            }
        }
        return tempInfo;
    }

    @Override
    public UserInfo addUser(UserInfo userInfo) {
        UUID uuid = UUID.randomUUID();
        UserInfo tempInfo = userInfo;
        tempInfo.setUuid(uuid);
        tempInfo = userInfoRepository.save(tempInfo);
        return tempInfo;
    }

    @Override
    public List<Meals> getMeals(){
        List<Meals> mealsList = new ArrayList<>();
        userMealsRepository.findAll().forEach(mealsList::add);
        return mealsList;
    }

    @Override
    public void setMeals(List<Meals> listMeals){
        userMealsRepository.saveAll(listMeals);
    }

    @Override
    public Integer addUserMealsOrder(UserMealsOrder order){
        idRepoOrder++;
        Integer orderNumber = idRepoOrder;
        UserMealsOrderDAO orderDB = convertUserMealsOrderToDAO(order);
        orderDB.setOrderNumber(orderNumber);
        userMealsOrderRepository.save(orderDB);
        numberOrder.setNumberOrder(orderNumber);
        userNumsOrderRepository.save(numberOrder);
        return orderNumber;
    }

    private OrderMeals convertMealsToOrderMeals(Meals meals){
        OrderMeals orderMeals = new OrderMeals();
        //orderMeals.setId(meals.getId());
        orderMeals.setTitle(meals.getTitle());
        orderMeals.setPrice(meals.getPrice());
        orderMeals.setDescription(meals.getDescription());
        orderMeals.setType(meals.getType());
        orderMeals.setLinkImage(meals.getLinkImage());
        orderMeals.setStatus(meals.getStatus());
        return orderMeals;
    }
    private Meals convertOrderMealsToMeals(OrderMeals orderMeals){
        Meals meals = new Meals();
        //meals.setId(orderMeals.getId());
        meals.setTitle(orderMeals.getTitle());
        meals.setPrice(orderMeals.getPrice());
        meals.setDescription(orderMeals.getDescription());
        meals.setType(orderMeals.getType());
        meals.setLinkImage(orderMeals.getLinkImage());
        meals.setStatus(orderMeals.getStatus());
        return meals;
    }

    private UserMealsOrderDAO convertUserMealsOrderToDAO(UserMealsOrder order) {
        UserMealsOrderDAO orderDB = new UserMealsOrderDAO();
        orderDB.setId(order.getId());
        orderDB.setOrderNumber(order.getOrderNumber());
        orderDB.setState(order.getState());
        orderDB.setTableNumber(order.getTableNumber());
        orderDB.setUserName(order.getUserName());
        orderDB.setUserUUID(order.getUserUUID());
        orderDB.setOrderTime(order.getOrderTime());
        orderDB.setBookingTime(order.getBookingTime());
        orderDB.setTotalPrice(order.getTotalPrice());
        List<OrderMeals> orderMealsList = new ArrayList<>();
        for(Meals meals:order.getOrderMeals()){
            orderMealsList.add(convertMealsToOrderMeals(meals));
        }
        orderDB.setOrderMeals(orderMealsList);
        return orderDB;
    }

    private UserMealsOrder convertDAOToUserMealsOrder(UserMealsOrderDAO  orderDB) {
        UserMealsOrder order = new UserMealsOrder();
        order.setId(orderDB.getId());
        order.setState(orderDB.getState());
        order.setOrderNumber(orderDB.getOrderNumber());
        order.setTableNumber(orderDB.getTableNumber());
        order.setUserName(orderDB.getUserName());
        order.setUserUUID(orderDB.getUserUUID());
        order.setOrderTime(orderDB.getOrderTime());
        order.setBookingTime(orderDB.getBookingTime());
        order.setTotalPrice(orderDB.getTotalPrice());
        List<Meals> mealsList = new ArrayList<>();
        for(OrderMeals meals:orderDB.getOrderMeals()){
            mealsList.add(convertOrderMealsToMeals(meals));
        }
        order.setOrderMeals(mealsList);
        return order;
    }

    @Override
    public UserMealsOrder getUserMealsOrder(Integer orderNumber) {
        if (orderNumber == null) return null;
        ArrayList<UserMealsOrderDAO> dbInfos = userMealsOrderRepository.findByOrderNumber(orderNumber);
        UserMealsOrderDAO tempInfo = null;
        for (UserMealsOrderDAO user:dbInfos) {
            if (orderNumber.equals(user.getOrderNumber())){
                tempInfo = user;
                break;
            }
        }
        return convertDAOToUserMealsOrder(tempInfo);
    }

    @Override
    public List<UserMealsOrder> getUserMealsOrder(UUID userUUID){
        if (userUUID == null) return new ArrayList<>();
        List<UserMealsOrderDAO> userMealsOrderDAO = userMealsOrderRepository.findByUserUUID(userUUID);
        List<UserMealsOrder> userMealsOrders = new ArrayList<>();
        for(UserMealsOrderDAO infoDAO: userMealsOrderDAO){
            userMealsOrders.add(convertDAOToUserMealsOrder(infoDAO));
        }
        return userMealsOrders;
    }

    @Override
    public void saveUserMealsOrder(UserMealsOrder order){
        userMealsOrderRepository.save(convertUserMealsOrderToDAO(order));
    }
    @Override
    public List<UserMealsOrder> getUserMealsOrderAll(){
        List<UserMealsOrder> userMealsOrders = new ArrayList<>();
        userMealsOrderRepository.findAll().forEach(x -> {
            userMealsOrders.add(convertDAOToUserMealsOrder(x));
        });
        return userMealsOrders;
    }

    @Override
    public List<UserMealsOrder> getUserMealsOrderByState(String state){
        List<UserMealsOrder> userMealsOrders = new ArrayList<>();
        userMealsOrderRepository.findByState(state).forEach(x -> {
            userMealsOrders.add(convertDAOToUserMealsOrder(x));
        });
        return userMealsOrders;
    }

    @Override
    public void removeBookingTable(int numberOfTable) {
        ArrayList<Booking> bookingDB = userBookingRepository.findByNumberOfTable(numberOfTable);
        if (bookingDB.size() > 0) {
            userBookingRepository.delete(bookingDB.get(0));
        }
    }

    @Override
    public List<Booking> getBookingList() {
        List<Booking> bookingList = new ArrayList<>();
        userBookingRepository.findAll().forEach(bookingList::add);
        return bookingList;
    }

    @Override
    public void addToBookingList(Booking booking) {
        userBookingRepository.save(booking);
    }

    @Override
    public List<DietTable> getTables(){
        List<DietTable> tableList = new ArrayList<>();
        userTableRepository.findAll().forEach(x->tableList.add(x));
        return tableList;
    }

    @Override
    public DietTable getTable(UUID userUuid) {
        ArrayList<DietTable> tableDB = userTableRepository.findByUserUUID(userUuid);
        if (tableDB.size() > 0) {
            return tableDB.get(0);
        }
        return null;
    }

    @Override
    public void saveTable(DietTable dietTable) {
        userTableRepository.save(dietTable);
    }
}
